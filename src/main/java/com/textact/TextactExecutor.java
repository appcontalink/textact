package com.textact;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;


import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.amazonaws.services.s3.model.S3Object;

import com.textact.client.TextactAws;
import com.textact.pdf.PagePdf;

import java.util.List;
import java.util.ArrayList;

import com.textact.banco.ScotiaBank;
import com.textact.banco.Citibanamex;
import com.textact.banco.Bancomer;
import com.textact.banco.Banorte;
import com.textact.banco.Banregio;
import com.textact.banco.Santander;
import com.textact.banco.Bajio;
import com.textact.banco.Banco;
import com.textact.banco.Inbursa;
import com.textact.banco.HSBC;
import com.textact.banco.Afirme;
import com.textact.banco.Azteca;

import com.textact.http.Request;
import com.textact.http.Response;
import com.textact.http.Connection;

import com.textact.db.EntityDatabase;
import com.textact.db.Query;
import com.textact.aws.AmazonBucket;
import java.util.UUID;
import com.textact.resources.Util;

import java.util.Calendar;


public class TextactExecutor {

 

  public void execute(InputStream inputStream, OutputStream outputStream) throws Exception {

    JSONObject request = (JSONObject) new JSONParser().parse(toString(inputStream));

    TextactAws textactClient = new TextactAws();

    JSONObject contextJson = (JSONObject) request.get("context");
    String  resourcePath =   (String) contextJson.get("resource-path");
    JSONObject bodyJson = (JSONObject) request.get("body-json");
    String httpMethod = (String) contextJson.get("http-method");

    JSONObject responseJSON = new JSONObject();
    JSONObject params = (JSONObject) request.get("params");
    JSONObject header = (JSONObject) params.get("header");
    JSONObject querystring = (JSONObject) params.get("querystring");

    String contentType = (String) header.get("Content-Type");



    if(resourcePath.equals("/textact/detect_text") || resourcePath.equals("/detect_text")){


      String bucket = (String) bodyJson.get("bucket");
      String key = (String) bodyJson.get("key");

      String jobId = textactClient.detectText(bucket, key);

      System.out.println(jobId);
      responseJSON.put("job_id", jobId);
      responseJSON.put("success", true);

      outputStream.write(responseJSON.toJSONString().getBytes(Charset.forName("UTF-8")));


    }else if(resourcePath.equals("/textact/get_document_text_detection") || resourcePath.equals("/get_document_text_detection")){

      String jobId =(String) bodyJson.get("job_id");
      String next_token = (String) bodyJson.get("next_token");

      JSONObject response = textactClient.getDocumentTextDetecion(jobId, next_token);
      //PagePdf pagePdf= new PagePdf(2, blocksJson);
      Boolean success = (Boolean) response.get("success");
      String banco= (String) bodyJson.get("banco");

      if(banco == null ){
        outputStream.write(response.toJSONString().getBytes(Charset.forName("UTF-8")));
        return;

      }

      if(success){

        List<PagePdf> pagesPdf=getPagePdfs(jobId, (JSONArray) response.get("blocks"), banco, (Integer) response.get("last_page"));
        JSONObject pdfs = new JSONObject();

        for(Integer page=0; page<pagesPdf.size(); page++ ){

          //System.out.println("IMPRIMIENTO LA PAGINA " + page);
          pdfs.put(  Integer.toString(page), pagesPdf.get(page).getObjectsJSON());
        }

        response.put("success", true);
        response.put("pages", pdfs);

      } 

      outputStream.write(response.toJSONString().getBytes(Charset.forName("UTF-8")));
      response.remove("blocks");
      System.out.println(response.toJSONString());


    }else if(resourcePath.equals("/textact/bancos") || resourcePath.equals("/banks")){
      JSONObject response = new JSONObject();

      JSONArray bancos = new JSONArray();

      bancos.add(Afirme.banco());
      bancos.add(Azteca.banco());

      bancos.add(Bancomer.banco());
      bancos.add(Banorte.banco());
      bancos.add(Banregio.banco());
      bancos.add(Bajio.banco());

      bancos.add(Citibanamex.banco());
      bancos.add(HSBC.banco());
      bancos.add(Inbursa.banco());

      bancos.add(Santander.banco());
      bancos.add(ScotiaBank.banco());

    
      response.put("success", true);
      response.put("bancos", bancos);
      outputStream.write(response.toJSONString().getBytes(Charset.forName("UTF-8")));

      }else if(resourcePath.equals("/textact/bancos") || resourcePath.equals("/banks")){
        JSONObject response = new JSONObject();
  
        JSONArray bancos = new JSONArray();
  
        bancos.add(Afirme.banco());
        bancos.add(Azteca.banco());
        
        bancos.add(Bancomer.banco());
        bancos.add(Banorte.banco());
        bancos.add(Banregio.banco());
        bancos.add(Bajio.banco());
  
        bancos.add(Citibanamex.banco());
        bancos.add(HSBC.banco());
  
        bancos.add(Santander.banco());
        bancos.add(ScotiaBank.banco());
  
      
        response.put("success", true);
        response.put("bancos", bancos);
        outputStream.write(response.toJSONString().getBytes(Charset.forName("UTF-8")));
  
        }else if(resourcePath.equals("/textact/get_transactions") || resourcePath.equals("/get_transactions")){

          String bancoName =(String) bodyJson.get("banco");
          JSONArray blocks =(JSONArray) bodyJson.get("blocks");
          String jobId =(String) bodyJson.get("jobId");



          PagePdf pagePdf= new PagePdf(jobId, blocks, bancoName);

  
          JSONObject json = new JSONObject();


          json.put("transactions", pagePdf.getObjectsJSON());



          outputStream.write(json.toJSONString().getBytes(Charset.forName("UTF-8")));
  
        } else if(resourcePath.equals("/accounts_status") && httpMethod.equals("POST")){
          try{
            EntityDatabase db = new EntityDatabase();
            db.setTable("account_status_pdf");

            bodyJson.remove("empresa_id");
            bodyJson.remove("usuario_id");

            db.openConnection();
            JSONObject item = db.save(bodyJson);
            db.closeConnection();
            outputStream.write(item.toJSONString().getBytes(Charset.forName("UTF-8")));

          } catch(Exception e){
            System.out.println(e.getMessage());
            
          }


        } else if(resourcePath.equals("/accounts_status/pdf")){

          System.out.println("REQUEST ------" + request.toJSONString());


          String name= (String) querystring.get("name");
          String bank= (String) querystring.get("bank");
          String email= (String) querystring.get("email");

          String uuid = UUID.randomUUID().toString();
          String content = (String) bodyJson.get("content");
          
          String key = "attachments/pdf/" +uuid +"/" + name;
          AmazonBucket bucket = new AmazonBucket();
          bucket.uploadPDF("cl-pdf-to-excel", key, bucket.toByteOutpueStream(content));
    
          String jobId = textactClient.detectText("cl-pdf-to-excel", key);



          System.out.println(jobId);
          JSONObject accountStatusPdf = new JSONObject();

          accountStatusPdf.put("name", name);
          accountStatusPdf.put("key", key);
          accountStatusPdf.put("bucket", "cl-pdf-to-excel");
          accountStatusPdf.put("job_id", jobId);
          accountStatusPdf.put("email", email);
          accountStatusPdf.put("banco", bank);
          accountStatusPdf.put("pdf", key);

          responseJSON.put("success", true);


          try{
            EntityDatabase db = new EntityDatabase();
            db.setTable("account_status_pdf");
            db.openConnection();
            accountStatusPdf = db.save(accountStatusPdf);
            db.closeConnection();
            responseJSON.put("account_status_pdf", accountStatusPdf);

          } catch(Exception e){
            System.out.println(e.getMessage());
            
          }


    
          outputStream.write(responseJSON.toJSONString().getBytes(Charset.forName("UTF-8")));

        }else if(resourcePath.equals("/accounts_status/xls")){

          Long id= Util.toLong((String) querystring.get("id"));

          try{


            //Obteniendo los valores el estado de cuenta del pdf
            EntityDatabase db = new EntityDatabase();
            db.setTable("account_status_pdf");
            db.openConnection();
            JSONObject accountStatusPdf = db.get(id);

            String content = (String) bodyJson.get("content");
            String uuid = UUID.randomUUID().toString();
            String name = (String) accountStatusPdf.get("name");
            String xls = "attachments/xls/" +uuid +"/" + name.replace(".pdf", ".xls").replace(".PDF", ".xls") ;
            AmazonBucket bucket = new AmazonBucket();
            bucket.uploadPDF("cl-pdf-to-excel", xls, bucket.toByteOutpueStream(content));


            //Enviando por correo
            Request req = new Request();
            req.setMethod(Request.POST);
            req.setUrl("https://vru6erjswd.execute-api.us-east-1.amazonaws.com/contalink_prod/cl-notifications-email/send-email");
            req.setAuthorization("89603753-5b67-4ac4-8962-fb3e9b294b58 f31323a3-d668-432b-b690-4d20857a2dda");
            req.setContentType("application/json");


            JSONObject item = new JSONObject();
            item.put("from", "contacto@contalink.com");
            item.put("subject", "Tu estado de cuenta en formato Excel de parte de Contalink.");
            item.put("html", "<html><head>    <style>        @import url('https://fonts.googleapis.com/css2?family=Manrope&display=swap');        .email {            font-family: Manrope, sans-serif;            width: 50%;            background-image: url(https://assets.website-files.com/60d627186db1a75e768b2d6a/60dc82877fcf53cd47564a12_contalink_bg_alpha2.png), url(https://assets.website-files.com/60d627186db1a75e768b2d6a/60ddfd533296a8e08abcf612_contalink_bg2.svg);            background-position: 100% 50%, 100% 50%;            background-size: cover, cover;            background-repeat: no-repeat, no-repeat;            background-attachment: scroll, fixed;            margin-left: auto;            margin-right: auto;        }        .contact-btn {            border-style: none;            background-color: rgba(228, 12, 12, 0.9);            color: rgb(255, 255, 255) !important;            padding: 12px 24px;            align-self: center;            border-radius: 4px;            font-size: 0.8rem;            line-height: 16px;            text-align: center;            letter-spacing: 0.6px;            text-decoration: none;            cursor: pointer;        }        .logo {            width: 60%;            height: auto;            margin: 0 auto;            position: relative;            padding-top: 30px;            padding-bottom: 20px;        }        .btndiv {            text-align: center;        }        .info {            padding-bottom: 30px;            text-align: justify;        }    </style></head><body>    <div class='email'>        <div class=logo>            <a href='https://www.contalink.com'>            <img src='https://s3.amazonaws.com/landing.contalink.com/assets/contalink_logo_hor.png'                alt='Contalink' width='100%'>            </a>        </div>        <div class='info'>            <p>Adjunto encontrarás el archivo en formato Excel generado a partir del PDF proporcionado</p>            <p>Esta es sólo una de las herramientas con las que Contalink puede ayudar a tu despacho u operación                contable a                reducir                tiempos y atender mejor a tus clientes.</p>            <p>Si deseas conocer más información, contáctanos</p>        </div>        <div class='btndiv'>            <a href='www.contalink.com/contact' class='contact-btn'>Contacto</a>        </div>        <div class='info'>            <br>            <br>        </div>    </div></body></html>");
            JSONArray correos = new JSONArray();
            correos.add((String) accountStatusPdf.get("email"));
            item.put("to", correos);

            JSONArray attachments = new JSONArray();
            JSONObject attachment = new JSONObject();
            attachment.put("bucket", (String) accountStatusPdf.get("bucket"));
            attachment.put("key", xls);
            attachments.add(attachment);
            item.put("attachments", attachments);
            req.setBody(item.toJSONString());

            //System.out.println(item.toJSONString());

            Connection conn = new Connection();
            Response response = conn.send(req);

            //System.out.println(response.getBody());
        
            JSONObject resultados = (JSONObject) new JSONParser().parse(response.getBody());
            accountStatusPdf.put("xls", xls);
            accountStatusPdf = db.update(accountStatusPdf);

            //System.out.println("TERMINO");


            db.closeConnection();
            responseJSON.put("success", true);
            outputStream.write(responseJSON.toJSONString().getBytes(Charset.forName("UTF-8")));
            //System.out.println("TERMINO 1");


          } catch(Exception e){
            System.out.println("ERROR" + e.getMessage());
            responseJSON.put("success", false);
            outputStream.write(e.getMessage().getBytes(Charset.forName("UTF-8")));


          }
          System.out.println("TERMINO 2");


        }else if(resourcePath.equals("/accounts_status/tries")){
          String email=(String) querystring.get("email");

          EntityDatabase db = new EntityDatabase();
          db.setTable("account_status_pdf");
          db.openConnection();


          String pattern = "yyyy-MM-dd";
          SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
          String dateNow = simpleDateFormat.format(new Date());

          Calendar calendar = Calendar.getInstance();
          calendar.add(Calendar.MONTH, -1);

          String datePrevious = simpleDateFormat.format(calendar.getTime());

          JSONArray items = db.get(
            Query.and(
              Query.eq("email", email),
              Query.ge("created_at",  datePrevious),
              Query.le("created_at", dateNow),
              Query.eq("estatus", "Exito")

               ), null, null, null);


          responseJSON.put("success", true);

          responseJSON.put("total", items.size());


          db.closeConnection();

          outputStream.write(responseJSON.toJSONString().getBytes(Charset.forName("UTF-8")));


        }else if(resourcePath.equals("/accounts_status/request")){

          String email=(String) bodyJson.get("email");
          String fileBase64 = (String) bodyJson.get("file");
          String name=(String) bodyJson.get("name");

          String bank=(String) bodyJson.get("bank");
          String message=(String) bodyJson.get("message");
          String uuid = UUID.randomUUID().toString();
          String empresId=(String) bodyJson.get("empresa_id");



          String key = "attachments/new_requests/" + name;
          if(fileBase64 != null){
            AmazonBucket bucket = new AmazonBucket();
            bucket.uploadPDF("cl-pdf-to-excel", key, bucket.toByteOutpueStream(fileBase64));
          }


          EntityDatabase db = new EntityDatabase();
          db.openConnection();

          db.setTable("account_status_pdf_request");

          JSONObject requestPdf=  new JSONObject();
          requestPdf.put("bucket","cl-pdf-to-excel" );
          requestPdf.put("key",key );
          requestPdf.put("name",name );
          requestPdf.put("bank",bank );
          requestPdf.put("email",email );
          requestPdf.put("empresa_id",empresId );


          requestPdf.put("message",message );

          requestPdf = db.save(requestPdf);


          db.closeConnection();

          outputStream.write(responseJSON.toJSONString().getBytes(Charset.forName("UTF-8")));


        }

        System.out.println("TERMINO 3");



    }


  private String toString(InputStream in) throws Exception {
    StringBuilder sb = new StringBuilder();
    BufferedReader br = new BufferedReader(new InputStreamReader(in));
    String read;

    while ((read = br.readLine()) != null) {
      sb.append(read);
    }

    br.close();
    return sb.toString();
  }


  private List<PagePdf> getPagePdfs(String jobId, JSONArray blocksJson, String banco, Integer numberPages){

    List<PagePdf> listsPages = new ArrayList<PagePdf>();

    for(Integer page=1; page<=numberPages; page++){

      PagePdf pagePdf= new PagePdf(jobId, page, blocksJson, banco);
      //System.out.println("TERMIO PAGE");
      listsPages.add(pagePdf);

    }


    return listsPages;

  }


}