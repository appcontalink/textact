package com.textact.aws;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.EnvironmentVariableCredentialsProvider;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;

import java.util.UUID;
import java.util.Base64;

public class AmazonBucket {
	private AmazonS3 s3Client;

	public AmazonBucket() {
		this.s3Client = new AmazonS3Client();
	}

	public AmazonBucket(String accessKey, String secretKey) {
		AWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);
		this.s3Client = new AmazonS3Client(credentials);

	}

	public void uploadXML(String bucketName, String path, ByteArrayOutputStream output)
	throws UnsupportedEncodingException {

		uploadXML(bucketName, path, output, false);


	}	


	public void uploadXML(String bucketName, String path, ByteArrayOutputStream output, Boolean is_attach)
			throws UnsupportedEncodingException {

		path = URLDecoder.decode(path, "UTF-8");

		InputStream input_xml = new ByteArrayInputStream(output.toByteArray());

		ObjectMetadata meta = new ObjectMetadata();
		meta.setContentLength(output.size());
		meta.setContentType("application/xml");
		String nombre = path.substring(path.lastIndexOf("/") + 1);
		if(is_attach){
			meta.setContentDisposition("attachment; filename=" + nombre);
		}

		//s3Client.putObject(bucketName, path, input_xml, meta);
		PutObjectRequest putObReq = new PutObjectRequest(bucketName, path, input_xml, meta).withCannedAcl(CannedAccessControlList.PublicRead);
		s3Client.putObject(putObReq);
		

	}
	
	
	
	public void uploadXLSX(String bucketName, String path, String nombre, ByteArrayOutputStream output)
			throws UnsupportedEncodingException {

		path = URLDecoder.decode(path, "UTF-8");

		InputStream input_xml = new ByteArrayInputStream(output.toByteArray());

		ObjectMetadata meta = new ObjectMetadata();
		meta.setContentLength(output.size());
		meta.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		meta.setContentDisposition("attachment; filename=" + nombre);

		//s3Client.putObject(bucketName, path, input_xml, meta);
		PutObjectRequest putObReq = new PutObjectRequest(bucketName, path, input_xml, meta).withCannedAcl(CannedAccessControlList.PublicRead);
		s3Client.putObject(putObReq);
		

	}
	

	public void uploadPDF(String bucketName, String path, ByteArrayOutputStream output)
			throws UnsupportedEncodingException {

		uploadPDF(bucketName, path, output, false);



	}


	public void uploadPDF(String bucketName, String path, ByteArrayOutputStream output, Boolean is_attach)
			throws UnsupportedEncodingException {

		path = URLDecoder.decode(path, "UTF-8");

		InputStream input_pdf = new ByteArrayInputStream(output.toByteArray());

		ObjectMetadata meta = new ObjectMetadata();
		meta.setContentLength(output.size());
		meta.setContentType("application/pdf");
		String nombre = path.substring(path.lastIndexOf("/") + 1);
		if(is_attach){
			meta.setContentDisposition("attachment; filename=" + nombre);
		}

		s3Client.putObject(bucketName, path, input_pdf, meta);

	}




	public void uploadPng(String bucketName, String path, ByteArrayOutputStream output)
			throws UnsupportedEncodingException {

		path = URLDecoder.decode(path, "UTF-8");

		InputStream input_pdf = new ByteArrayInputStream(output.toByteArray());

		ObjectMetadata meta = new ObjectMetadata();
		meta.setContentLength(output.size());
		meta.setContentType("image/png");

		s3Client.putObject(bucketName, path, input_pdf, meta);

	}


	// http://docs.ceph.com/docs/master/radosgw/s3/java/
	public InputStream download(String bucketName, String path) throws UnsupportedEncodingException {

		path = URLDecoder.decode(path, "UTF-8");

		//ObjectMetadata meta = new ObjectMetadata();
		//meta.setContentEncoding("UTF-8");

		S3Object s3object = this.s3Client.getObject(new GetObjectRequest(bucketName, path));
		InputStream inputStream = s3object.getObjectContent();

		return inputStream;

	}


	public void uploadZIP(String bucketName, String path, ByteArrayOutputStream output)
			throws Exception {
		
		//OutputStream outputStream = new FileOutputStream("./file/"+ path.replace(".xml", ".zip")); 
		//output.writeTo(outputStream);
		
		System.out.println("Subiendo el archivo");

		
		path=path.replace(".xml", ".zip");
		path = URLDecoder.decode(path, "UTF-8");

		InputStream input_zip = new ByteArrayInputStream(output.toByteArray());

		ObjectMetadata meta = new ObjectMetadata();
		meta.setContentLength(output.size());
		meta.setContentType("application/zip");

		s3Client.putObject(bucketName, path, input_zip, meta);

	}


	public ByteArrayOutputStream toByteOutpueStream(String base64){
		try {
            byte[] decodedString = Base64.getDecoder().decode(base64.getBytes("UTF-8"));
			ByteArrayOutputStream baos = new ByteArrayOutputStream(decodedString.length);
			baos.writeBytes(decodedString);

			return baos;
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
        }

		return null;
	}

}
