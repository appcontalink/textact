package com.textact.pdf;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;


import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;


import java.util.Collections;

import com.textact.banco.Banco;
import com.textact.banco.ScotiaBank;
import com.textact.banco.Citibanamex;
import com.textact.banco.Bancomer;
import com.textact.banco.Banorte;
import com.textact.banco.Banregio;
import com.textact.banco.Santander;
import com.textact.banco.Bajio;
import com.textact.banco.Inbursa;
import com.textact.banco.Afirme;
import com.textact.banco.HSBC;
import com.textact.banco.Azteca;

import com.textact.resources.Util;

public class PagePdf {

    private JSONArray texts = new JSONArray();
    private Integer page;

    private Banco banco = null;

    List<BoundingBoxPDF> headers = new ArrayList<BoundingBoxPDF>();

    private String[] titles;

    private String bank_name;
    private Float topHeader;
    List<Float> valuesTop = new ArrayList<Float>();

    private List<RowPDF> rows = new ArrayList<RowPDF>();
    Map<String, String> dictKeys = new HashMap<String, String>();

    public static final Map<String, List<BoundingBoxPDF>> jobs = new HashMap<String, List<BoundingBoxPDF>>();

    public PagePdf(String jobId, JSONArray blocksJson, String bank_name) {
        this(jobId, null, blocksJson, bank_name);
    }

    public PagePdf(String jobId, Integer page, JSONArray blocksJson, String bank_name) {

        // System.out.println("PAGINA NO: " + page);

        this.bank_name = bank_name;
        // System.out.println("Banco");
        this.setBanco(jobId);
        // System.out.println("keya");

        //blocksJson = this.adjustBlocks(blocksJson);


        for (Integer i = 0; i < blocksJson.size(); i++) {
            JSONObject blockJson = (JSONObject) blocksJson.get(i);

            if (page != null) {

                Integer pageText = (Integer) blockJson.get("page");
                if (page == pageText) {
                    this.texts.add(blockJson);
                    this.banco.addBlock(blockJson);

                }
            } else {
                this.texts.add(blockJson);
                this.banco.addBlock(blockJson);
            }

        }


        this.banco.blocks = blocksJson;

        this.titles = this.banco.getTitles();
        dictKeys = this.banco.getKeys();

        for (Integer i = 0; i < blocksJson.size(); i++) {
            JSONObject blockJson = (JSONObject) blocksJson.get(i);
            if (page != null) {
                Integer pageText = (Integer) blockJson.get("page");
                if (page == pageText) {
                    this.searchHeaderTitles(blockJson);

                }
            } else {
                this.searchHeaderTitles(blockJson);
            }

        }

        // System.out.println("Blocks");

        // System.out.println(texts.toJSONString());

        this.page = page;
        this.deleteDuplicates();

        // Obtiene el valores de concepto porque es el principal
        this.topHeader = this.banco.beginTableOne(this.headers, this.banco.getTitleLeader());
        if (this.topHeader >= 1.0f) {
            this.topHeader = this.banco.beginTableSecond(this.texts);
            if (this.topHeader < 1) {
                // System.out.println("SE ENCONTRO EN EL DICCIOARIO");
                this.headers = this.jobs.get(jobId);
                for (Integer i = 0; i < this.headers.size(); i++) {
                    this.headers.get(i).setTop(this.topHeader);

                }
            } else {
                return;
            }
        } else {

            // Obtiene los valores del encabezado
            List<BoundingBoxPDF> newHeaders = new ArrayList<BoundingBoxPDF>();
            for (BoundingBoxPDF box : this.headers) {
                // System.out.println("PROCESO DE BUSQUEDA DE HEADER" + box.key());

                if (box.getTop() > (topHeader - this.banco.getBottomHeader())
                        && box.getTop() < (topHeader + this.banco.getTopHeader())) {
                    // System.out.println("TIENE LA MISMA MEDIDA DEL TOP HEADER" + box.key());
                    newHeaders.add(box);
                }
            }

            this.headers = newHeaders;
            this.orderHeaders();

            if (!PagePdf.jobs.containsKey(jobId)) {
                // System.out.println("COLOCADO DICITIONARIO");
                PagePdf.jobs.put(jobId, this.headers);
            }

        }

        // System.out.println("TOPHEADER" + this.topHeader);

        for (Integer i = 0; i < this.headers.size(); i++) {

            RowPDF row = new RowPDF();
            row.setHeader(this.headers.get(i));
            row.setTop(this.headers.get(i).getTop() + this.headers.get(i).getHeight());
            row.setHeight(1 -row.getTop());
            // System.out.println(this.headers.get(i).key());

            // Colocando las medidas del ROW

            this.headers.get(i)
                    .setWidth(this.headers.get(i).getWidth() + this.banco.getWidth(this.headers.get(i).getText()));

            if (i == 0) {
                // System.out.println("ENTROOO AQUI: " + this.headers.get(i).getText());
                row.setLeft(0.00f);
                row.setWidth(this.headers.get(i).getLeft() + this.headers.get(i).getWidth());
            } else if (i < this.headers.size() - 1) {
                row.setLeft(this.headers.get(i - 1).getLeft() + this.headers.get(i - 1).getWidth());
                row.setWidth(this.headers.get(i).getLeft() + this.headers.get(i).getWidth() - row.getLeft());
            } else if (i == this.headers.size() - 1) {
                row.setLeft(this.headers.get(i - 1).getLeft() + this.headers.get(i - 1).getWidth());
                row.setWidth(1 - (this.headers.get(i - 1).getLeft() + this.headers.get(i - 1).getWidth()));
            }

            // System.out.println(this.headers.get(i).key());
            // System.out.println("ROW------" + row.key());

            // Buscando los datos

            for (Integer j = 0; j < this.texts.size(); j++) {
                JSONObject textJson = (JSONObject) this.texts.get(j);
                JSONObject geometry = (JSONObject) textJson.get("geometry");
                JSONObject boundingBox = (JSONObject) geometry.get("bounding_box");

                Float left = Util.toFloat(boundingBox.get("left"));
                Float width = Util.toFloat(boundingBox.get("width"));
                Float top = Util.toFloat(boundingBox.get("top"));
                Float height = Util.toFloat(boundingBox.get("height"));
                // System.out.println("textJson: " + textJson.get("text") + "?left=" + left +
                // "&width=" + width);

                Float center = left;
                if (textJson.get("text") != null && row != null && left != null) {
                    if (this.banco.isCenter(row.getText())) {
                        center = left + (width / 2);
                    }

                    if (center > row.getLeft()
                            && center < (row.getLeft() + row.getWidth())
                            && top > row.getTop()) {

                        // System.out.println("right" +this.headers.get(i).getLeft());
                        // System.out.println("left" + (this.headers.get(i).getLeft() +
                        // this.headers.get(i).getWidth()));
                        //System.out.println("SET TEXT: " + (String) textJson.get("text") + "width: " + width + " left: " + left + " top: " + top);

                        BoundingBoxPDF box = new BoundingBoxPDF();
                        box.setLeft(roundOff(left, 3));
                        box.setHeight(roundOff(height, 3));
                        box.setTop(roundOff(top, 3));
                        box.setWidth(roundOff(width, 3));
                        box.setText((String) textJson.get("text"));
                        row.addCell(box);

                    }
                }
            }

            this.rows.add(row);
        }

        Map<String, Float> keysTop = new HashMap<String, Float>();

        for (RowPDF row : this.rows) {
            for (BoundingBoxPDF cell : row.getCells()) {
                String keyTop = Float.toString(cell.getTop());
                if (!keysTop.containsKey(keyTop)) {
                    keysTop.put(keyTop, cell.getTop());
                    // System.out.println("DATO: " + cell.getText());
                }
            }
        }

        this.valuesTop = new ArrayList<Float>(keysTop.values());
        Collections.sort(valuesTop);

        // Borrar los que estan cercas del previo
        List<Float> newValuesTop = new ArrayList<Float>();
        for (Integer i = 0; i < this.valuesTop.size(); i++) {
            newValuesTop.add(this.valuesTop.get(i));
        }

        this.valuesTop = newValuesTop;
        for (RowPDF row : this.rows) {
            row.buildCells(this.banco, newValuesTop);
        }

    }

    public JSONArray getObjectsJSON() {

        List<JSONObject> items = new ArrayList<JSONObject>();
        JSONArray itemsJson = new JSONArray();
        // System.out.println("GET OBJECT JSON ");

        // Es para quitar las ultimas lineas que no corresponden a la tabla
        Boolean moreLoop = true;

        for (Integer index = 0; index < this.valuesTop.size() && moreLoop; index++) {

            JSONObject item = new JSONObject();

            for (RowPDF row : this.rows) {
                // System.out.println("SET KEY: " + row.getText());
                item.put(this.dictKeys.get(row.getText()), row.getTextCell(index));
                item.put("y", this.valuesTop.get(index));

            }

            moreLoop = isValidFormatToContinue(item);
            if (moreLoop) {
                item = removeWords(item);
                if (item != null) {
                    items.add(item);
                }
            }
        }


        //System.out.println(items.toJSONString());


        List<JSONObject> elimitatedItems = new ArrayList<JSONObject>();
        if (items.size() > 0) {
            elimitatedItems.add(items.get(0));

            if (items.size() > 1) {

                for (Integer index = 0; index < items.size(); index++) {
                    
                    JSONObject currentItem = items.get(index);
                    Float currentTop = (Float) currentItem.get("y");
                    Float previousTop = 0.00f;
                    JSONObject previousItem =null;

                    String currentSaldo = (String) currentItem.get("saldo");
                    if(currentSaldo == null){
                        currentSaldo="";
                    }
                    String previousSaldo = "";

                    if(elimitatedItems.size() > 0){
                        previousItem = elimitatedItems.get(elimitatedItems.size()-1);
                        previousTop = (Float) previousItem.get("y");
                        previousSaldo = (String) previousItem.get("saldo");

                        if(previousSaldo == null){
                            previousSaldo="";
                        }
                    }

                    //System.out.println("---------");
                    //System.out.println("currentTop: " + currentTop);
                    //System.out.println("previousTop: " + previousTop);

                    if ((previousTop + 0.004f) >= currentTop) {
                        //System.out.println("hacer el merge");

                        if(previousItem != null && currentItem != null){
                            System.out.println("ENTROOO AQUI CURRENT" + currentItem.toJSONString());
                            System.out.println("ENTROOO AQUI PREVIOUS" + previousItem.toJSONString());

                            elimitatedItems.set(elimitatedItems.size()-1, mergeJson(previousItem, currentItem));
                        }

                    } else if(!previousSaldo.equals("") && !currentSaldo.equals("") && currentSaldo.equals(previousSaldo)) {
                        //No hace nada
                    }else {
                        elimitatedItems.add(currentItem);
                    }

                }

            }

        }


        for(JSONObject item : elimitatedItems){
            itemsJson.add(item);
        }


        System.out.println(itemsJson.toJSONString());


        return itemsJson;

    }

    public JSONObject mergeJson(JSONObject json1, JSONObject json2) {


        JSONObject result = new JSONObject();


        String fecha1 = (String) json1.get("fecha");
        String fecha2 = (String) json2.get("fecha");

        if(fecha1 != null){

            if (fecha1.length() > fecha2.length()) {
                result.put("fecha", fecha1);
            }
            if (fecha2.length() > fecha1.length()) {
                result.put("fecha", fecha2);
            } else {
                result.put("fecha", fecha1);

            }
        }



        Float top = (Float) json1.get("y");
        result.put("y", top);

        String concepto1 = (String) json1.get("concepto");
        String concepto2 = (String) json2.get("concepto");

        if(concepto1 != null){

            if (concepto1.length() > concepto2.length()) {
                result.put("concepto", concepto1);
            }
            if (concepto2.length() > concepto1.length()) {
                result.put("concepto", concepto2);
            } else {
                result.put("concepto", concepto1);

            }
        }

        String saldo1 = (String) json1.get("saldo");
        String saldo2 = (String) json2.get("saldo");
        
        if(saldo1 !=  null){
            if (saldo1.length() > saldo2.length()) {
                result.put("saldo", saldo1);
            }
            if (saldo2.length() > saldo1.length()) {
                result.put("saldo", saldo2);
            } else {
                result.put("saldo", saldo1);

            }
        }

        String retiro1 = (String) json1.get("retiro");
        String retiro2 = (String) json2.get("retiro");

        if(retiro1 != null){
            if (retiro1.length() > retiro2.length()) {
                result.put("retiro", retiro1);
            }
            if (retiro2.length() > retiro1.length()) {
                result.put("retiro", retiro2);
            } else {
                result.put("retiro", retiro1);

            }
        }

        String deposito1 = (String) json1.get("deposito");
        String deposito2 = (String) json2.get("deposito");

        if(deposito1 !=null){
            if (deposito1.length() > deposito2.length()) {
                result.put("deposito", deposito1);
            }
            if (deposito2.length() > deposito1.length()) {
                result.put("deposito", deposito2);
            } else {
                result.put("deposito", deposito1);

            }
        }

        System.out.println(result.toJSONString());

        return result;
    }

    public JSONArray getKeys() {
        JSONArray keys = new JSONArray();

        return keys;

    }

    public JSONArray getDetectionsTexts() {
        return this.texts;
    }

    private void searchHeaderTitles(JSONObject titleJson) {
        if (titleJson.get("text") != null) {

            String text = (String) titleJson.get("text");
            text = text.toLowerCase().trim();
            // System.out.println("BUSCANDO TITULOS" + text);

            for (Integer i = 0; i < titles.length; i++) {

                if (text.equals(titles[i])) {

                    // System.out.println("Añadiendo titulo" + text);

                    BoundingBoxPDF box = new BoundingBoxPDF();

                    box.setText(text);
                    JSONObject geometry = (JSONObject) titleJson.get("geometry");
                    JSONObject boundingBox = (JSONObject) geometry.get("bounding_box");
                    box.setTop(roundOff(Util.toFloat(boundingBox.get("top")), 3));
                    box.setWidth(roundOff(Util.toFloat(boundingBox.get("width")), 3));
                    box.setLeft(roundOff(Util.toFloat(boundingBox.get("left")), 3));
                    box.setHeight(roundOff(Util.toFloat(boundingBox.get("height")), 3));
                    this.headers.add(box);

                }
            }

        }

    }

    private void deleteDuplicates() {
        Map<String, BoundingBoxPDF> dict = new HashMap<String, BoundingBoxPDF>();

        List<BoundingBoxPDF> news = new ArrayList<BoundingBoxPDF>();

        for (BoundingBoxPDF box : this.headers) {
            if (!dict.containsKey(box.key())) {
                news.add(box);
                dict.put(box.key(), box);
                // System.out.println("key: " + box.key());

            } else {
                BoundingBoxPDF current = dict.get(box.key());
            }
        }

        this.headers = news;

    }

    public void print() {

        for (BoundingBoxPDF box : this.headers) {
            // System.out.println(box.key());
        }
    }

    private float roundOff(float x, int position) {
        float a = x;
        double temp = Math.pow(10.0, position);
        a *= temp;
        a = Math.round(a);
        return (a / (float) temp);
    }

    // Debe ser un formato correcto de fecha para poder seguir
    private Boolean isValidFormatToContinue(JSONObject item) {
        return this.banco.isValidFormatToContinue(item);

    }

    // Este metodo es para mover palabras repetitivas

    private JSONObject removeWords(JSONObject item) {
        return this.banco.adjustJson(item);
    }

    private void setBanco(String jobId) {

        if (this.bank_name.equals("citibanamex")) {
            this.banco = new Citibanamex(jobId);
        } else if (this.bank_name.equals("scotiabank")) {
            this.banco = new ScotiaBank(jobId);
        } else if (this.bank_name.equals("bancomer")) {
            this.banco = new Bancomer(jobId);
        } else if (this.bank_name.equals("banorte")) {
            this.banco = new Banorte(jobId);
        } else if (this.bank_name.equals("banregio")) {
            this.banco = new Banregio(jobId);
        } else if (this.bank_name.equals("santander")) {
            this.banco = new Santander(jobId);
        } else if (this.bank_name.equals("bajio")) {
            this.banco = new Bajio(jobId);
        } else if (this.bank_name.equals("inbursa")) {
            this.banco = new Inbursa(jobId);
        } else if (this.bank_name.equals("afirme")) {
            this.banco = new Afirme(jobId);
        } else if (this.bank_name.equals("hsbc")) {
            this.banco = new HSBC(jobId);
        } else if (this.bank_name.equals("azteca")) {
            this.banco = new Azteca(jobId);
        }

    }

    public void orderHeaders() {
        List<Float> order = new ArrayList<Float>();
        for (BoundingBoxPDF box : this.headers) {
            order.add(box.getLeft());
        }

        Collections.sort(order);

        List<BoundingBoxPDF> newHeaders = new ArrayList<BoundingBoxPDF>();

        for (Float left : order) {
            for (BoundingBoxPDF box : this.headers) {
                if (box.getLeft() == left) {
                    newHeaders.add(box);
                    // System.out.println(box.key());
                }
            }
        }

        this.headers = newHeaders;

    }

  

}