package com.textact.http;

import java.util.HashMap;
import java.io.File;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;

public class Request {

	public static String POST = "POST";
	public static String GET = "GET";
	public static String DELETE = "DELETE";
	public static String PUT = "PUT";

	String contentType;
	String body;
	String method;
	HashMap<String, String> param = new HashMap<String, String>();
	String url;
	String soapAction;
	String paramsCadena;
	List<File> files = new ArrayList<File>();
	String xAuthToken;
	String xrefid;
	String contentMD5;
	String authorization;

	String contentLength;

	Map<String, String> headers = new HashMap<String, String>();

	public Request() {
		param = new HashMap<String, String>();
		files = new ArrayList<File>();
	}

	public String getAuthorization(){
		return this.authorization;
	}

	public void setAuthorization(String authorization){
		this.authorization= authorization;
	}


	public String getContentLength(){
		return this.contentLength;
	}

	public void setContentLength(String contentLength){
		this.contentLength= contentLength;
	}

	/**
	 * @return the contentType
	 */
	public String getContentType() {
		return contentType;
	}

	/**
	 * @return the method
	 */
	public String getMethod() {
		return method;
	}

	/**
	 * @param method
	 *            the method to set
	 */
	public void setMethod(String method) {
		this.method = method;
	}

	/**
	 * @return the param
	 */
	public HashMap<String, String> getParam() {
		return param;
	}

	/**
	 * @param param
	 *            the param to set
	 */
	public void addParam(String key, String value) {
		param.put(key, value);
	}

	/**
	 * @param contentType
	 *            the contentType to set
	 */
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	/**
	 * @return the body
	 */
	public String getBody() {
		return body;
	}

	/**
	 * @param body
	 *            the body to set
	 */
	public void setBody(String body) {
		this.body = body;
	}

	
	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url
	 *            the url to set
	 */
	public void setUrl(String url) {

		System.out.println(url);
		this.url = url;
	}

	/**
	 * @return the soapAction
	 */
	public String getSoapAction() {
		return soapAction;
	}

	/**
	 * @param soapAction
	 *            the soapAction to set
	 */
	public void setSoapAction(String soapAction) {
		this.soapAction = soapAction;
	}

	public String getxAuthToken() {
		return xAuthToken;
	}

	public void setxAuthToken(String xAuthToken) {
		this.xAuthToken = xAuthToken;
	}

	public String getXrefid() {
		return xrefid;
	}

	public void setXrefid(String xrefid) {
		this.xrefid = xrefid;
	}

	public String getContentMD5() {
		return contentMD5;
	}

	public void setContentMD5(String contentMD5) {
		this.contentMD5 = contentMD5;
	}
	


			/**
	 * @return the authorization
	 */
	public Map<String, String> getHeaders() {
		return this.headers;
	}

	/**
	 * @return the method
	 */
	public void setHeaders(Map<String, String> headers) {
		 headers = headers;
	}

	

}