package com.textact.db;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import java.util.List;
import java.util.Iterator;
import org.json.simple.JSONArray;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


public class Util {

	public static String readFile(File file) throws FileNotFoundException {
		String text = new Scanner(file, "UTF-8").useDelimiter("\\A").next();
		return text;
	}

	public static String find(String regexpresion, String text) throws SQLException {

		String validar = "\\(*" + regexpresion + "\\)*";
		text=text.trim();

		if (!text.matches(validar)) {
			throw new SQLException("The expression" + text + " is not valid");
		}

		Pattern pattern = Pattern.compile(regexpresion);
		Matcher matcher = pattern.matcher(text);
		if (matcher.find()) {
			return matcher.group(0);
		}

		return null;
	}
	
	public static boolean isNumber( String text) throws SQLException {


		if (!text.matches("\\d+")) {
			throw new SQLException("The parameter " + text + " is not number");
		}

	

		return true;
	}
	
	
	public static String toString(InputStream in) throws IOException {
		StringBuilder sb = new StringBuilder();
		BufferedReader br = new BufferedReader(new InputStreamReader(in));
		String read;

		while ((read = br.readLine()) != null) {
			sb.append(read);
		}

		br.close();
		return sb.toString();
	}

	
	public static String reservedWord(String name) {

        if (name.equals("table"))
            return "\"table\"";


        else if (name.equals("default"))
            return "\"default\"";

        else if (name.equals("transaction"))
            return "\"transaction\"";
        else if (name.equals("window"))
            return "\"window\"";
        else if (name.equals("from"))
            return "\"from\"";
        else if (name.equals("name"))
            return "\"name\"";
        else if (name.equals("to"))
            return "\"to\"";
        return name;


    }


    public static Double toDouble(Object obj){

        if(obj instanceof Long ){
            Long value = (Long) obj;
            return Double.valueOf(value);
        } else if(obj instanceof Integer){
            return Double.valueOf((Integer) obj);
        }else if(obj instanceof Double){
            return (Double) obj;
        }

        return null;
    }


    public static Long toLong(Object obj){

        if(obj instanceof Long ){
            return (Long) obj;
        } else if(obj instanceof Integer){
            return Long.valueOf((Integer) obj);
        } else if(obj instanceof String){
            return Long.valueOf((String) obj);
        }

        return null;
    }


    

	public static String buildSets(List<Column> columns, JSONObject obj){

    	String update = "";

        for(Iterator iterator = obj.keySet().iterator(); iterator.hasNext();){

            String keyJson= (String) iterator.next();
            String params[]= new String[2];
            Boolean has_point=false;

            for (Column col : columns)
            {
                String keyCol = col.getName();
                String type= col.getType();


                if(keyJson.contains(".") && type.equals("json")){
                    params= keyJson.split("\\.");
                    keyJson = params[0]; 
                    has_point= true; 

                }


                if (keyJson.equals(keyCol))
                {

                    update = update + " " + reservedWord(keyCol) + "=";

                    if(has_point && type.equals("json")){

                        

                        String valor = (String) obj.get(col.getName());
                        String jsonUpdate = "util_json_object_set_key(" +params[0] + ", '" + params[1] + "', '" +valor +"')";  
                        update = update + jsonUpdate + ",";

                    }
                    else if (type.equals("json"))
                    {

                        Object objClass = obj.get(col.getName());

                        String valor = null;
                        if(objClass == null)
                        {
                            valor = " null ";
                            update = update + valor + ",";
                        }
                        else if(objClass instanceof JSONObject)
                        {
                            JSONObject data_json = (JSONObject) obj.get(col.getName());

                            if(data_json.containsKey("null")){
                                data_json.remove("null");
                            }

                            valor = data_json.toJSONString();
                            valor =valor.replace("'", "''" );
                            update = update + "'" + valor + "'::JSON" + ",";

                        }
                        else if(objClass instanceof JSONArray)
                        {
                            JSONArray data_json = (JSONArray) obj.get(col.getName());

                            valor = data_json.toJSONString();
                            valor =valor.replace("'", "''" );
                            valor = valor.replace("\n", "");
                            update = update + "'" + valor + "'" + ",";

                        }

                    }
                    else
                    {
                        Object objClass = obj.get(col.getName());

                        if (objClass == null)
                        {
                            update = update + "null" + ",";
                        }
                        else if (objClass instanceof String)
                        {


                            String value = (String) objClass;
                            if (value.equals("null"))
                            {
                                update = update + obj.get(col.getName()) + ",";
                            }
                            else
                            {
                                update = update + "'" + value.replace("\'", "\'\'") + "'" + ",";
                            }


                        }
                        else if (objClass instanceof Long)
                        {
                            Long value = (Long) objClass;
                            update = update + "'" + String.valueOf(value) + "'" + ",";

                        }
                        else if (objClass instanceof Boolean)
                        {
                            Boolean value = (Boolean) objClass;
                            update = update + "'" + String.valueOf(value) + "'" + ",";

                        }
                        else if (objClass instanceof Double)
                        {
                            Double value = (Double) objClass;
                            update = update + "'" + String.valueOf(value) + "'" + ",";
                        }
                        else if (objClass instanceof Integer)
                        {
                            Integer value = (Integer) objClass;
                            update = update + "'" + String.valueOf(value) + "'" + ",";
                        }

                    }

                }
            }
        }

        update = update.substring(0, update.length() - 1);

        return update;

    }


    public static String toString(JSONObject objJson, String key) {

		Object obj = objJson.get(key);
		if (obj == null) {
			return null;
		}

		if (obj instanceof String) {
			return (String) obj;

		} else if (obj instanceof Long) {
			Long l = (Long) obj;
			return l.toString();
		} else if (obj instanceof Integer) {
			Integer l = (Integer) obj;
			return l.toString();
		} else if (obj instanceof Double) {
			Double d = (Double) obj;
			return d.toString();
		} 


		return null;
	}


    public static boolean validaterRFC(String rfc) {

		if (rfc == null)
			return false;

		if(rfc.equals("")){
			return false;
		}

		rfc = rfc.trim().toUpperCase();
		if (rfc.matches("[A-Z,Ñ,&]{3,4}[0-9]{2}[0-1][0-9][0-3][0-9][A-Z0-9]{3}")) {
			return true;
		}

		return false;
	}



 

}
