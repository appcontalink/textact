package com.textact.client;

import com.amazonaws.services.textract.model.Document;
import com.amazonaws.services.textract.AmazonTextractAsync;
import com.amazonaws.services.textract.AmazonTextractAsyncClientBuilder;

import com.amazonaws.services.textract.model.DetectDocumentTextRequest;
import com.amazonaws.services.textract.model.DetectDocumentTextResult;
import com.amazonaws.services.textract.model.AnalyzeDocumentRequest;
import com.amazonaws.services.textract.model.AnalyzeDocumentRequest;
import com.amazonaws.services.textract.model.S3Object;

import com.amazonaws.services.textract.model.StartDocumentTextDetectionRequest;
import com.amazonaws.services.textract.model.DocumentLocation;
import com.amazonaws.services.textract.model.StartDocumentTextDetectionResult;
import java.util.concurrent.Future;

import com.amazonaws.services.textract.model.GetDocumentTextDetectionRequest;
import com.amazonaws.services.textract.model.GetDocumentTextDetectionResult;
import com.amazonaws.services.textract.model.Block;
import com.amazonaws.services.textract.model.Geometry;
import com.amazonaws.services.textract.model.Point;
import com.amazonaws.services.textract.model.BoundingBox;
import com.amazonaws.client.builder.AwsClientBuilder.EndpointConfiguration;

import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.textact.db.EntityDatabase;




public class TextactAws {

    static Boolean LOCAL= false;

    private AmazonTextractAsync client;

    public TextactAws(){

        if(LOCAL){
            this.client = AmazonTextractAsyncClientBuilder.defaultClient();

        } else{
            EndpointConfiguration endpoint = new EndpointConfiguration("https://textract.us-east-1.amazonaws.com", "us-east-1");
            this.client = AmazonTextractAsyncClientBuilder.standard()
                .withEndpointConfiguration(endpoint).build();
        }
    }

    public String detectText(String bucket, String name) {
        S3Object s3Object = new S3Object().withBucket(bucket).withName(name);
        DocumentLocation documentLocation= new DocumentLocation().withS3Object(s3Object);
        StartDocumentTextDetectionRequest request =new  StartDocumentTextDetectionRequest().withDocumentLocation(documentLocation);
        Future<StartDocumentTextDetectionResult> future = this.client.startDocumentTextDetectionAsync(request);

        try {
            while(!future.isDone()) {
                //System.out.println("Calculating...");
                Thread.sleep(300);
            }
            StartDocumentTextDetectionResult result = future.get();

            return result.getJobId();



            
        } catch(java.lang.InterruptedException e){

            System.out.println(e.getMessage());

        } catch(java.util.concurrent.ExecutionException e){
            System.out.println(e.getMessage());


        }

        return null;


        
    }


    public void analizeDocument(String bucket, String name){
        S3Object s3Object = new S3Object().withBucket(bucket).withName(name);
        Document document= new Document().withS3Object(s3Object);
        AnalyzeDocumentRequest request = new AnalyzeDocumentRequest().withDocument(document);


    }


    public JSONObject getDocumentTextDetecion(String jobId, String nextToken){

        JSONObject response= new JSONObject();
        GetDocumentTextDetectionRequest request= null;

        if(nextToken != null){
            request= new GetDocumentTextDetectionRequest().withJobId(jobId).withNextToken(nextToken);

        }else {
            request= new GetDocumentTextDetectionRequest().withJobId(jobId);
        }


        Future<GetDocumentTextDetectionResult> future = this.client.getDocumentTextDetectionAsync(request);
        try {
            while(!future.isDone()) {
                //System.out.println("Calculating...");
                Thread.sleep(300);
            }
            GetDocumentTextDetectionResult result = future.get();

            String statusMessage = result.getStatusMessage();
            String jobStatus =  result.getJobStatus();

         
            response.put("status_message", statusMessage);
            response.put("job_status", jobStatus);
            response.put("success", false);


            if(jobStatus.equals("SUCCEEDED")){


                Integer lastPage=0;



                List<Block> blocks = result.getBlocks();
                JSONArray blocksJson= new JSONArray();

                for(Block block: blocks){

                    JSONObject blockJson= new JSONObject();
                    blockJson.put("text", block.getText());
                    blockJson.put("page", block.getPage());

                    if(lastPage < block.getPage() ){
                        lastPage = block.getPage();
                    }

                    blockJson.put("text_type", block.getTextType());
                    blockJson.put("row_index", block.getRowIndex());
                    blockJson.put("row_span", block.getRowSpan());
                    blockJson.put("column_index", block.getColumnIndex());
                    blockJson.put("column_span", block.getColumnSpan());
                    blockJson.put("block_type", block.getBlockType());
                    blockJson.put("id", block.getId());

                    if( block.getGeometry() != null){
                        Geometry geometry = block.getGeometry();
                        JSONObject geometryJson= new JSONObject();

                        List<Point> points = geometry.getPolygon();
                        JSONArray pointsJson = new JSONArray();

                        for(Point point: points){
                            JSONObject pointJson= new JSONObject();
                            pointJson.put("x", point.getX());
                            pointJson.put("y", point.getY());
                            pointsJson.add(pointJson);
                            
                        }

                        geometryJson.put("polygon",pointsJson );


                        if(geometry.getBoundingBox() != null){
                            BoundingBox boundingBox = geometry.getBoundingBox() ;
                            JSONObject boundingBoxJson= new JSONObject();

                            boundingBoxJson.put("height", boundingBox.getHeight());
                            boundingBoxJson.put("left", boundingBox.getLeft());
                            boundingBoxJson.put("top", boundingBox.getTop());
                            boundingBoxJson.put("width", boundingBox.getWidth());
                            geometryJson.put("bounding_box", boundingBoxJson);


                        }

                        blockJson.put("geometry", geometryJson);

        
                    }


            
                    blocksJson.add(blockJson);

                }

                response.put("blocks", blocksJson);
                response.put("next_token", result.getNextToken());
                response.put("success", true);
                response.put("last_page", lastPage);

            }




            return response;
            
        } catch(java.lang.InterruptedException e){

            System.out.println("EXCEPTION "+  e.getMessage());


        } catch(java.util.concurrent.ExecutionException e){
            System.out.println("EXCEPTION "+  e.getMessage());

        }

        return new JSONObject();
    }


}