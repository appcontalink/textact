package com.textact.banco;
import com.textact.banco.Banco;

import java.util.Map;
import java.util.HashMap;

import org.json.simple.JSONObject;
import org.json.simple.JSONArray;

import java.util.Map;
import java.util.HashMap;
import com.textact.resources.Util;


public class Azteca  extends Banco {

    public static String[] MESES = { " ene ", " feb ", " mar ", " abr ", " may "  , " jun ", " jul ", " ago " , " sep ", " oct ", " nov ", " dic "};




    public static JSONObject banco(){
        JSONObject banco = new JSONObject();
        banco.put("value", "azteca");
        banco.put("label", "Azteca");
        banco.put("active", true);

        return banco;

    }

    public Azteca(String jobId){
        super(jobId);

    }

    public void addBlock(JSONObject item){
        String text = (String) item.get("text");

  


    }


    public String[] getTitles(){
        String[] titles=  { "fecha", "concepto", "monto de la operación", "Lugar o Canal de"};

        return titles;
    }

    public String[] getTitlesHeader(){
        String[] titles=  { "fecha", "concepto", "monto de la operación", "lugar o canal de" };

        return titles;
    }


    public String getTitleLeader(){
        return "concepto";
    }


    public Map<String, String> getKeys(){

        Map<String, String> keys= new HashMap<String, String>();
 
        keys.put("concepto", "concepto");
        keys.put("fecha", "fecha");
        keys.put("monto de la operación", "monto");
        keys.put("lugar o canal de", "canal");

        return keys;

    }

    public String isDataType(String key, String value){

        value= value.trim();

        System.out.println("key: " + key  + " value:" + value);

     


        return value;
    }


    public JSONObject adjustJson(JSONObject json){

        //System.out.println("INICIO --- "+json.toJSONString());

        String fecha =  (String) json.get("fecha");

        if(fecha != null){

            fecha= fecha.trim();
            if(fecha.indexOf(" ") > 0){
                String fechaTemp = Util.getFirsttWord(fecha);
                json.put("fecha", fechaTemp);

                String concepto = fecha.replace(fechaTemp, "");
                json.put("concepto", concepto);

            }

          
        }

        String monto = (String) json.get("monto");
        if(monto != null){
            if(monto.contains("(+)")){ 

                monto = monto.replace("(+)", "").trim();
                monto = Util.getFirsttWord(monto);
                monto = Util.convertNumberTwoDecimals(monto);
                json.put("monto", null);
                json.put("deposito", monto);
                json.put("retiro", "");


            }else if(monto.contains("(-)")){
                
                monto = monto.replace("(-)", "").trim();
                monto = Util.getFirsttWord(monto);

                monto = Util.convertNumberTwoDecimals(monto);
                json.put("monto", null);
                json.put("retiro", monto);
                json.put("deposito", "");


            }else {
                json.put("deposito", "");
                json.put("retiro", "");

            }
        }

        json.remove("monto");


        //System.out.println("FIN --- "+json.toJSONString());



        return json;
     
    }

    public Boolean isValidFormatToContinue(JSONObject item){

        System.out.println("item: " + item.toJSONString());

        String concepto = (String) item.get("concepto");

        if(concepto.contains("Continúa en la siguiente hoja")){
            return false;
        }

   

        return true;
    }




}