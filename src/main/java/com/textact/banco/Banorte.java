package com.textact.banco;

import com.textact.banco.Banco;

import java.util.Map;
import java.util.HashMap;

import org.json.simple.JSONObject;

import com.textact.resources.Util;

public class Banorte extends Banco {

    // A veces se pueden encontrar mas cuentas con los mismo titulos
    private Boolean findDescription = false;

    String[] isDeposito= { "recibido", "traspaso", "efectivo", "cheque"};
    String[] isRetiro= {"pago", "comision", "traspaso"};

    private String descripcion = "descripción/establecimiento";

    private String[] posiblesDescriptions = {
            "fecha descripción /establecimiento",
            "fecha descripción /establecimiento",
            "fecha escripción/establecimiento",
            "fecha scripción/establecimiento",
            "fecha descripción/establecimie",
            "iscripción/establecimiento",
            "fecha ripción/establecimiento",
            "fecha iripción/establecimiento",
            "fecha cripción /establecimiento",
            "fecha descripción /establecimient",
            "fecha iipción/establecimiento",
            "fecha descripción / establecimiento",
            "fecha ión/establecimiento",
            "fecha descripción/establecimiento" };

    public static JSONObject banco() {
        JSONObject banco = new JSONObject();
        banco.put("value", "banorte");
        banco.put("label", "Banorte");
        banco.put("active", true);

        return banco;

    }

    public Banorte(String jobId) {
        super(jobId);

        // System.out.println("HELLO");

    }

    public void addBlock(JSONObject item) {
        String text = (String) item.get("text");

        // System.out.println("TEX BANORTE: " + text);
        if (text != null && !findDescription) {
            text = text.trim().toLowerCase();

            for (Integer i = 0; i < posiblesDescriptions.length; i++) {
                if (text.equals(posiblesDescriptions[i])) {
                    findDescription = true;
                    descripcion = posiblesDescriptions[i];

                }
            }

            if (text.contains("establecimient")) {
                this.descripcion = text;
                findDescription = true;
            }

        }

    }

    public String[] getTitles() {
        // System.out.println("titulos");

        if (descripcion.contains("fecha")) {
            String[] titles = { descripcion, "monto del deposito", "monto del retiro", "monto del deposito monto del retiro",
                    "saldo" };
            return titles;
        } else {
            String[] titles = { "fecha", descripcion, "monto del deposito", "monto del retiro", "monto del deposito monto del retiro",
                    "saldo" };
            return titles;
        }

    }

    public String[] getTitlesHeader() {

        String[] titles = { "fecha", "descripción/establecimiento", "monto del deposito", "monto del retiro", "saldo" };
        return titles;
    }

    public String getTitleLeader() {

        return descripcion;
    }

    public Map<String, String> getKeys() {

        Map<String, String> keys = new HashMap<String, String>();

        for (Integer i = 0; i < posiblesDescriptions.length; i++) {
            keys.put(posiblesDescriptions[i], "concepto");

        }

        keys.put(this.descripcion, "concepto");

        keys.put("descripción/establecimiento", "concepto");

        keys.put("fecha", "fecha");
        keys.put("monto del deposito", "deposito");
        keys.put("monto del retiro", "retiro");
        keys.put("deposito", "deposito");
        keys.put("retiro", "retiro");
        keys.put("saldo", "saldo");
        keys.put("monto del deposito", "deposito");
        keys.put("monto del deposito monto del retiro", "deposito_retiro");

        return keys;

    }

    public String isDataType(String key, String value) {
        System.out.println("isDataType " + key + ": " + value);

        value = value.trim();

        if (key.equals("monto del deposito") || key.equals("monto del retiro")
                || key.equals("saldo") || key.equals("deposito") || key.equals("retiro") || key.equals("monto del deposito monto del retiro")) {

            // System.out.println(key + ": " + value);

            String regex = "^\\d*\\.\\d+|\\d+\\.\\d*$";
            value = value.trim();
            value = value.replace("$", "").replace(",", "");

            if (Util.numberWords(value) == 2) {
                return Util.getLastWord(value);
            }

            if (value.contains("-") && (key.equals("monto del retiro") || key.equals("retiro"))) {
                return "-" + value.replace("-", "");
            }

            if (value.matches(regex)) {
                return value;
            } else {
                return "";
            }

        } else if (key.equals("fecha")) {
            value = value.trim();

            if (!value.equals("")) {

                Integer space = value.indexOf(" ");

                if (space > -1) {
                    return value.substring(0, space);
                } else {
                    return value;
                }

            }

        }

        return value;
    }

    public JSONObject adjustJson(JSONObject json) {

        System.out.println("JSON OBJECT: " + json.toJSONString());

        String concepto = (String) json.get("concepto");

        if (this.descripcion.indexOf("fecha") > -1) {
            json = buildFecha(json);
        }

        String fecha = (String) json.get("fecha");
        if (fecha != null) {
            fecha = Util.changesZero(fecha);
            json.put("fecha", fecha);
        }

        String retiro = (String) json.get("retiro");
        String deposito = (String) json.get("deposito");

        if (retiro != null) {
            if (!retiro.equals("")) {

                retiro = retiro.trim();
                json.put("retiro", retiro.replace(",", ""));

                if (retiro.contains("-")) {
                    json.put("retiro", "");
                    json.put("deposito", retiro.replace("-", ""));

                }

            }
        }

        if (deposito != null) {
            if (!deposito.equals("")) {
                deposito = deposito.trim();
                json.put("deposito", deposito.replace(",", ""));

            }
        }

        String saldo = (String) json.get("saldo");
        saldo = saldo.trim();


        if (saldo.contains("0.00") && concepto.contains("SALDO ANTERIOR")) {
            return null;
        }

        json.put("deposito", Util.convertNumberTwoDecimals((String) json.get("deposito")));

        /*
         * if(Util.reviewFechawithAmount(json)){
         * return null;
         * }
         */

        fecha = (String) json.get("fecha");

        if (!fecha.equals("")) {
            if (fecha.length() > 9) {

                concepto = (String) json.get("concepto");
                String inicio = fecha.substring(9);
                concepto = inicio + " " + (String) json.get("concepto");
                json.put("concepto", concepto);

                fecha = fecha.substring(0, 9);

                json.put("fecha", fecha);

            }

        }

        concepto= (String) json.get("concepto");
        if(Util.hasMonthWithGuion(concepto)){
            fecha = concepto.substring(0, 9);
            json.put("fecha", fecha);
            concepto =concepto.replace(fecha, "");
            json.put("concepto", concepto);


        }

        System.out.println("JSON OBJECT FINAL: " + json.toJSONString());

        return json;
    }

    public Boolean isValidFormatToContinue(JSONObject item) {
        // System.out.println("JSON" + item.toJSONString());

        String saldo = (String) item.get("saldo");
        String concepto = (String) item.get("concepto");

        if (this.descripcion.indexOf("fecha") > -1) {
            item.put("fecha", "");
        }

        if (concepto != null) {
            if (concepto.indexOf("En Banorte estamos para servirte") > -1) {
                return false;
            } else if (concepto.indexOf("SIN MOVIMIENTOS") > -1) {
                return false;
            } else if (concepto.contains("Línea Directa")) {
                return false;
            }
        }

        String fecha = (String) item.get("fecha");

        if (fecha == null) {
            return false;
        }
        fecha = fecha.trim();

        if (fecha.equals("")) {

            return true;
        } else if (Util.hasMonth(fecha)) {
            return true;
        }

        return false;
    }

    public JSONObject buildFecha(JSONObject item) {
        String concepto = (String) item.get("concepto");

        if (concepto != null) {
            if (concepto.indexOf(" ") > -1) {
                Integer space = concepto.indexOf(" ");
                String fecha = concepto.substring(0, space);
                if (Util.hasMonthWithGuion(fecha)) {
                    item.put("fecha", fecha);
                    item.put("concepto", concepto.replace(fecha, "").trim());
                    return item;

                }

            }
        }

        // item.put("fecha", "");

        return item;

    }

    public Float getWidth(String key) {

        if (key.equals(this.getTitleLeader())) {
            return 0.08f;

        }

        return 0.0f;

    }

}