package com.textact.pdf;

import com.textact.pdf.BoundingBoxPDF;

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;

import java.util.regex.Pattern;
import com.textact.banco.Banco;

public class RowPDF extends BoundingBoxPDF {
    

    private BoundingBoxPDF header;
    private List<BoundingBoxPDF> cells = new ArrayList<BoundingBoxPDF>();

    private List<String> texts= new ArrayList<String>();
    private Banco banco;
    public Float positionX;

    public RowPDF(){
        super();
    }

    public void setHeader(BoundingBoxPDF header ){
        this.header=header;
        this.texto = header.getText();
    }


    public BoundingBoxPDF getHeader(){
        return this.header;

    } 


    public void addCell(BoundingBoxPDF cell){
        this.cells.add(cell);
    }


    public List<BoundingBoxPDF> getCells(){

        return this.cells;

    }


    public void printCells(){

        System.out.println("-----------------------HEADER" + this.texto );
        for(BoundingBoxPDF box: this.cells){
            System.out.println(box.key());
        }

        System.out.println("-----------------------" );

    }


    public String getTextCell(Integer index){
        return this.texts.get(index);

    }

    public void buildCells(Banco banco, List<Float> keysTop){

        this.banco= banco;

        for(Integer index=0; index<keysTop.size(); index++){
            
            Float top = keysTop.get(index);

            String result="";
            Float leftCell = 0.00f;

            for(BoundingBoxPDF cell : this.cells){
                Float topCell = cell.getTop();

                if(topCell >= top  &&  topCell <= top+0.002  ){
                    if(leftCell <= cell.getLeft() ){
                        leftCell = cell.getLeft() + cell.getWidth();
                        result= result +  " "+ cell.getText();
                        if(positionX == null){
                            this.positionX = topCell;
                        }
                    }
                }

            }


            result= isDataType(result);

            this.texts.add(result);

        }


    }


    public void printTexts(){


        System.out.println("-----------------------HEADER" + this.texto );
        for(String text: this.texts){
            System.out.println(text);
        }

        System.out.println("-----------------------" );

    }




    public String isDataType(String value){

        return this.banco.isDataType(this.texto, value);

    }



}



