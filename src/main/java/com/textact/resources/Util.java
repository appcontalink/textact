package com.textact.resources;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.json.simple.JSONObject;



public class Util {


    public static String[] MESES = { "ene", "feb", "mar", "abr", "may" , "jun", "jul", "ago", "sep", "oct", "nov", "dic"};



    public static Boolean hasMonth(String fecha){

        if(fecha == null) {
            return false;
        }else if(fecha.equals("")){
            return false;
        }

        fecha = changesZero(fecha);

        return fecha.contains("ENE") || fecha.contains("FEB") 
        || fecha.contains("MAR") ||  fecha.contains("ABR") || fecha.contains("MAY")
        || fecha.contains("JUN") || fecha.contains("JUL") || fecha.contains("AGO") || fecha.contains("AG0")
        || fecha.contains("SEP") || fecha.contains("OCT") || fecha.contains("0CT") || fecha.contains("NOV") || fecha.contains("N0V")
        || fecha.contains("DIC");

    }



    public static Boolean isMonth(String fecha){

        if(fecha == null) {
            return false;
        }else if(fecha.equals("")){
            return false;
        }

        fecha= fecha.toUpperCase();

        return fecha.contains("ENE") || fecha.contains("FEB") 
        || fecha.contains("MAR") ||  fecha.contains("ABR") || fecha.contains("MAY")
        || fecha.contains("JUN") || fecha.contains("JUL") || fecha.contains("AGO") || fecha.contains("AG0")
        || fecha.contains("SEP") || fecha.contains("OCT") || fecha.contains("0CT") || fecha.contains("NOV") || fecha.contains("N0V")
        || fecha.contains("DIC");

    }



    public static Boolean isMonthComplete(String fecha){

        if(fecha == null) {
            return false;
        }else if(fecha.equals("")){
            return false;
        }

        fecha= fecha.toUpperCase();

        return fecha.contains("ENERO") || fecha.contains("FEBRERO") 
        || fecha.contains("MARZO") ||  fecha.contains("ABRIL") || fecha.contains("MAYO")
        || fecha.contains("JUNIO") || fecha.contains("JULIO") || fecha.contains("AGOSTO") || fecha.contains("AG0STO")
        || fecha.contains("SEPTIEMBRE") || fecha.contains("OCTUBRE") || fecha.contains("0CTUBRE") || fecha.contains("NOVIEMBRE") || fecha.contains("N0VIEMBRE")
        || fecha.contains("DICIEMBRE");

    }


    public static Boolean hasMonthWithGuion(String fecha){

        fecha = changesZero(fecha);

        if(fecha == null) {
            return false;
        }else if(fecha.equals("")){
            return false;
        }


        return fecha.contains("-ENE-") || fecha.contains("-FEB-") || fecha.contains("-MAR-") ||  fecha.contains("-ABR-") || fecha.contains("-MAY-")
        || fecha.contains("-JUN-") || fecha.contains("-JUL-") || fecha.contains("-AGO-") | fecha.contains("-AG0-")
        || fecha.contains("-SEP-") || fecha.contains("-0CT-") || fecha.contains("-OCT-") || fecha.contains("-NOV-")  || fecha.contains("-N0V-")
        || fecha.contains("-DIC-");

    }


    public static Boolean hasMonthNumberWithGuion(String fecha){

        fecha = changesZero(fecha);

        if(fecha == null) {
            return false;
        }else if(fecha.equals("")){
            return false;
        }


        return fecha.contains("-01-") || fecha.contains("-02-") || fecha.contains("-03-") ||  fecha.contains("04-") || fecha.contains("-05-")
        || fecha.contains("-06-") || fecha.contains("-07-") || fecha.contains("-08-") | fecha.contains("-09-")
        || fecha.contains("-10-") || fecha.contains("-11-") || fecha.contains("-12-");

    }


    public static Boolean hasMonthWithDiagonal(String fecha){

        if(fecha == null) {
            return false;
        }else if(fecha.equals("")){
            return false;
        }


        return fecha.contains("/ENE") || fecha.contains("/FEB") || fecha.contains("/MAR") ||  fecha.contains("/ABR") || fecha.contains("/MAY")
        || fecha.contains("/JUN") || fecha.contains("/JUL") || fecha.contains("/AGO") | fecha.contains("/AG0")
        || fecha.contains("/SEP") || fecha.contains("/0CT") || fecha.contains("/OCT") || fecha.contains("/NOV")  || fecha.contains("/N0V")
        || fecha.contains("/DIC");

    }



    public static String changesZero(String value){
        return value.replace("0CT", "OCT").replace("AG0", "AGO" ).replace("N0V", "NOV");
    }


    public static String getLastWord(String text){
        if(text != null){
            text = text.trim();
            if(!text.equals("") && text.indexOf(" ") > -1){
                String[] words = text.split(" ");
                return words[words.length-1]; 
            }
        }

        return text;
    }


    public static String getFirsttWord(String text){
        if(text != null){
            text = text.trim();
            if(!text.equals("") && text.indexOf(" ") > -1){
                String[] words = text.split(" ");
                return words[0]; 
            }
        }

        return text;
    }



    public static Float toFloat(Object obj){

        if(obj instanceof Float){
            return (Float) obj;
        }else if(obj instanceof Double){
            Double point = (Double)obj;
            return point.floatValue();
        }

        return null;


    }


    public static Boolean isNumber(String number){

        number= number.trim();
        try{
            BigDecimal decimal = new BigDecimal(number);
            return true;
        } catch (Exception  e){
            return false;
        }

    }


    public static String convertNumber(String number){

        String regex = "^\\d*\\.\\d+|\\d+\\.\\d*$";
        number= number.trim();
        number= number.replace("$", "").replace(",","" ).replace("MX", "");
        number= number.trim();

        if(number.matches(regex)){
            return number.trim();
         }else {
             return "";
         }

    }
    public static String convertNumberTwoDecimals(String number){

        if(number == null){
            return "";
        }

        String regex = "^\\d*\\.\\d+|\\d+\\.\\d*$";
        number= number.trim();
        number= number.replace("$", "").replace(",","" ).replace("MX", "");
        if(number.matches(regex)){
            number = number.trim();


            BigDecimal d = new BigDecimal(number);
            BigDecimal result = d.subtract(d.setScale(0, RoundingMode.FLOOR)).movePointRight(d.scale());   


            if(result.doubleValue() > 100){
                return "";
            }



            return number.trim();
         }else {
             return "";
         }

    }

    public static Integer numberWords(String text){
        if(text != null){
            text = text.trim();
            if(!text.equals("") && text.indexOf(" ") > -1){
                String[] words = text.split(" ");
                return words.length; 
            }else if(!text.equals("")){
                return 1;
            }
        }

        return 0;

    }


    public static String getWord(String text, Integer index){
        if(text != null){
            text = text.trim();
            if(!text.equals("") && text.indexOf(" ") > -1){
                String[] words = text.split(" ");
                return words[index]; 
            }
        }

        return "";

    }



    public static boolean reviewFechawithAmount(JSONObject json){

        String fecha = (String) json.get("fecha");
        String concepto = (String) json.get("concepto");

        if(!fecha.equals("") && !concepto.equals("")){
            String deposito = (String) json.get("deposito");
            String retiro =  (String) json.get("retiro");
            if(deposito.equals("") && retiro.equals("")){
                return true;
            }
        }

        return false;
    }

    
    public static Long toLong(Object obj){

        if(obj instanceof Long ){
            return (Long) obj;
        } else if(obj instanceof Integer){
            return Long.valueOf((Integer) obj);
        } else if(obj instanceof String){
            return Long.valueOf((String) obj);
        }

        return null;
    }


    public static String getMonthNumber(String mes){

        for(Integer i=0; i< MESES.length; i++){
            if(mes.contains(MESES[i])){
                if((i+1) < 10){
                    return "0" + (i+1);
                }
            }
        }

        return "";
    } 



    public static Integer count(String message, char letra){    
        int count = 0;    
            
        //Counts each character except space    
        for(int i = 0; i < message.length(); i++) {    
            if(message.charAt(i) == letra)    
                count++;    
        }
        return count;
    
            
           
    }

    public static Integer containsYear(String text) {
        
        if(text.contains("enero del")){
            return text.lastIndexOf("enero del") + "enero del".length();
        } else if(text.contains("febrero del")){
            return text.lastIndexOf("febrero del") + "febrero del".length();
        }else if(text.contains("marzo del")){
            return text.lastIndexOf("marzo del") + "marzo del".length();
        }else if(text.contains("abril del")){
            return text.lastIndexOf("abril del") + "abril del".length();
        }else if(text.contains("mayo del")){
            return text.lastIndexOf("mayo del") + "mayo del".length();
        }else if(text.contains("junio del")){
            return text.lastIndexOf("junio del") + "junio del".length();
        }else if(text.contains("julio del")){
            return text.lastIndexOf("julio del") + "julio del".length();
        }else if(text.contains("agosto del")){
            return text.lastIndexOf("agosto del") + "agosto del".length();
        }else if(text.contains("septiembre del")){
            return text.lastIndexOf("septiembre del") + "septiembre del".length();
        }else if(text.contains("octubre del")){
            return text.lastIndexOf("octubre del") + "octubre del".length();
        }else if(text.contains("noviembre del")){
            return text.lastIndexOf("noviembre del") + "noviembre del".length();
        }else if(text.contains("diciembre del")){
            return text.lastIndexOf("diciembre del") + "diciembre del".length();
        }


        return -1;
    }


    public static String  containsMonth(String text) {
        
        if(text.contains("enero del")){
            return "01";
        } else if(text.contains("febrero del")){
            return "02";
        }else if(text.contains("marzo del")){
            return "03";
        }else if(text.contains("abril del")){
            return "04";
        }else if(text.contains("mayo del")){
            return "05";
        }else if(text.contains("junio del")){
            return "06";
        }else if(text.contains("julio del")){
            return "07";
        }else if(text.contains("agosto del")){
            return "08";
        }else if(text.contains("septiembre del")){
            return "09";
        }else if(text.contains("octubre del")){
            return "10";
        }else if(text.contains("noviembre del")){
            return "11";
        }else if(text.contains("diciembre del")){
            return "12";
        }


        return "";
    }
    

    public static String  replaceMonth(String text) {
        
        if(text.contains("ENE")){
            return text.replace("ENE", "01");
        } else if(text.contains("FEB")){
            return text.replace("FEB", "02");
        }else if(text.contains("MAR")){
            return text.replace("MAR", "03");
        }else if(text.contains("ABR")){
            return text.replace("ABR", "04");
        }else if(text.contains("MAY")){
            return text.replace("MAY", "05");
        }else if(text.contains("JUN")){
            return text.replace("JUN", "06");
        }else if(text.contains("JUL")){
            return text.replace("JUL", "07");
        }else if(text.contains("AGO")){
            return text.replace("AGO", "08");
        }else if(text.contains("SEP")){
            return text.replace("SEP", "09");
        }else if(text.contains("OCT")){
            return text.replace("OCT", "10");
        }else if(text.contains("NOV")){
            return text.replace("NOV", "11");
        }else if(text.contains("DIC")){
            return text.replace("DIC", "12");
        }


        return text;
    }

}