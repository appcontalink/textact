package com.textact.pdf;

import com.amazonaws.services.textract.model.BoundingBox;

public class BoundingBoxPDF extends  BoundingBox{

    public String texto;

    public BoundingBoxPDF(){
        super();
    }

    public void setText(String text){
        this.texto= text;
    }

    public String getText(){
        return this.texto;
    }

    public String key(){

        return this.texto + "?top" + this.getTop() + "&width=" + this.getWidth() + "&left=" + this.getLeft() + "&height"  + this.getHeight(); 

    }

    

    



}