package com.textact.banco;

import com.textact.banco.Banco;

import java.util.Map;
import java.util.Calendar;
import java.util.HashMap;
import com.textact.resources.Util;

import org.json.simple.JSONObject;

public class Bajio extends Banco {

    private String descripcion = "descripcion de la operacion";
    private String anio = null;

    public static JSONObject banco() {
        JSONObject banco = new JSONObject();
        banco.put("value", "bajio");
        banco.put("label", "Bajio");
        banco.put("active", true);

        return banco;

    }

    public Bajio(String jobId) {

        super(jobId);
        this.anio = this.recoverYear();

    }

    public void addBlock(JSONObject item) {
        String text = (String) item.get("text");
        if (text != null) {
            text = text.trim().toLowerCase();
            // System.out.println("BAJIO: " + text);

            if (text.indexOf("Periodo") > -1) {
                System.out.println(text);

            }

            if (text.equals("descripcion")) {
                this.descripcion = "descripcion";
            }

            if (this.anio == null) {

                text = text.toUpperCase();

                if (Util.isMonthComplete(text)) {
                    this.anio = text.substring(text.length() - 4);
                    save(anio);

                }
            }

        }

    }

    public String[] getTitles() {
        String[] titles = { "fecha", "no. ref.", "docto", this.descripcion, "depositos", "retiros", "saldo" };

        return titles;
    }

    public String[] getTitlesHeader() {
        String[] titles = { "fecha", "no. ref. docto", "descripcion", "depositos", "retiros", "saldo" };

        return titles;
    }

    public String getTitleLeader() {
        return this.descripcion;
    }

    public Map<String, String> getKeys() {

        Map<String, String> keys = new HashMap<String, String>();
        keys.put("descripcion de la operacion", "concepto");
        keys.put("fecha", "fecha");
        keys.put("retiros", "retiro");
        keys.put("depositos", "deposito");
        keys.put("saldo", "saldo");
        keys.put("descripcion de la operacion", "concepto");
        keys.put("descripcion", "concepto");
        keys.put("docto", "docto");

        return keys;

    }

    public String isDataType(String key, String value) {

        // System.out.println(key + ": " + value);

        if (key.equals("saldo") && value.indexOf(" ") > -1) {
            value = value.replace("$", "").replace(",", "");
            value = value.trim();

            if (value.indexOf(" ") > -1) {
                System.out.println(value);
                return value;
            }

        }

        if (key.equals("depositos") || key.equals("retiros") || key.equals("saldo")) {

            if (value != null) {
                if (value.indexOf("$") > -1) {
                    value = value.substring(value.indexOf("$"));

                }
            }

            if (value.contains("SALDO TOTAL*")) {
                System.out.println("hola");
                return "END";
            }

            String regex = "^\\d*\\.\\d+|\\d+\\.\\d*$";
            value = value.trim();
            value = value.replace("$", "").replace(",", "").replace(" ", "").replace("D", "");

            if (value.matches(regex)) {
                return value;
            } else {
                return "";
            }

        }

        return value;
    }

    public JSONObject adjustJson(JSONObject json) {

        if (this.anio == null) {
            int year = Calendar.getInstance().get(Calendar.YEAR);
            this.anio = String.valueOf(year);

        }

        String fecha = (String) json.get("fecha");
        if (Util.numberWords(fecha) == 1) {
            return null;
        }

        // System.out.println("JSON-> " + json.toJSONString());

        String concepto = (String) json.get("concepto");
        json.put("concepto", concepto.trim());

        fecha = (String) json.get("fecha");
        fecha = fecha.trim();
        if (fecha.indexOf(" ") > -1) {
            String[] datosFecha = fecha.split(" ");
            if (datosFecha[0].length() == 1) {
                datosFecha[0] = "0" + datosFecha[0];
            }
            json.put("fecha", datosFecha[0] + "/" + datosFecha[1]);

        }

        String saldo = (String) json.get("saldo");
        if (saldo != null) {
            saldo = saldo.trim();
            if (saldo.indexOf(" ") > -1) {
                String[] datos = saldo.split(" ");
                json.put("retiro", datos[0]);
                json.put("saldo", datos[1]);

            }
        }

        if (json.containsKey("docto")) {
            String docto = (String) json.get("docto");
            concepto = (String) json.get("concepto");

            if (docto != null && concepto != null) {
                json.put("concepto", docto + " " + concepto);

            }
        }

        fecha = (String) json.get("fecha");
        if (!fecha.equals("")) {
            fecha = Util.replaceMonth(fecha) + "/" + this.anio;
            json.put("fecha", fecha);
        }

        String deposito = (String)json.get("deposito");
        String retiro = (String)json.get("retiro");
         concepto =(String ) json.get("concepto");

         if(concepto.contains("deposito") && !deposito.equals("") && !retiro.equals("")){
            json.put("retiro", "");

         }
 


        return json;
    }

    public Boolean isValidFormatToContinue(JSONObject item) {

 
        // System.out.println("isValidFormatToContinue" + item.toJSONString());

        String fecha = (String) item.get("fecha");

        String retiro = (String) item.get("retiro");
        String deposito = (String) item.get("deposito");
        String concepto = (String) item.get("concepto");

        if(concepto.contains("NO EXISTEN CARGOS OBJETADOS")){
            return false;
        }


        if (deposito != null) {
            if (deposito.equals("END")) {
                return false;
            }

        }

        /*
         * if(deposito != null && retiro != null && concepto != null && fecha != null){
         * if(deposito.equals("") && concepto.equals("") && retiro.equals("") &&
         * fecha.equals("")){
         * System.out.println("Ya  no continua 1");
         * return false;
         * }
         * 
         * }
         */

        if (fecha == null) {
            System.out.println("1");
            return false;
        }
        fecha = fecha.trim();

        if (fecha.equals("")) {
            return true;
        } else if (fecha.contains("ENE") || fecha.contains("FEB")
                || fecha.contains("MAR") || fecha.contains("ABR") || fecha.contains("MAY")
                || fecha.contains("JUN") || fecha.contains("JUL") || fecha.contains("AGO")
                || fecha.contains("SEP") || fecha.contains("OCT") || fecha.contains("NOV")
                || fecha.contains("DIC")) {
            return true;
        }

        if (retiro != null) {
            if (retiro.contains("USD")) {
                return false;
            }
        }

        if (deposito != null) {
            if (deposito.contains("USD")) {

                return false;
            }
        }

        if (Util.isNumber(fecha)) {
            item.put("fecha", "");
            return true;
        }

        System.out.println("4");

        return false;
    }

    public Double getBottomHeader() {
        return 0.004;

    }

    public Double getTopHeader() {
        return 0.008;

    }

}