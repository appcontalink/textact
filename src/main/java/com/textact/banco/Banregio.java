package com.textact.banco;
import com.textact.banco.Banco;
import com.textact.resources.Util;

import java.util.Map;
import java.util.HashMap;

import org.json.simple.JSONObject;
import org.json.simple.JSONArray;

public class Banregio  extends Banco {

    public static String[] MESES = { "enero", "febrero", "marzo", "abril", "mayo" , "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre"};

    public Integer mes= null;
    public String anio="";


    public Boolean previuosHasConcepto =true;


    public static JSONObject banco(){
        JSONObject banco = new JSONObject();
        banco.put("value", "banregio");
        banco.put("label", "Banregio");
        banco.put("active", true);

        return banco;

    }

    public Banregio(String jobId){
        super(jobId);

    }

    public void addBlock(JSONObject item){
        String text = (String) item.get("text");

        if(text != null){
            text = text.toLowerCase().trim();
            //System.out.println("TEXT: " + text);
            for(Integer i=0; i< MESES.length; i++){
                if(text.indexOf(MESES[i]) > -1 && mes == null){

                    mes=i;
                    this.anio = text.substring(text.length() -4);
                }
            }
        }




    }


    public String[] getTitles(){
        String[] titles=  { "dia", "concepto", "cargos", "abonos", "saldo"};

        return titles;
    }

    public String[] getTitlesHeader(){
        String[] titles=  { "dia", "concepto", "cargos", "abonos", "saldo"};

        return titles;
    }


    public String getTitleLeader(){
        return "concepto";
    }


    public Map<String, String> getKeys(){

        Map<String, String> keys= new HashMap<String, String>();
 
        keys.put("concepto", "concepto");
        keys.put("dia", "fecha");
        keys.put("abonos", "deposito");
        keys.put("cargos", "retiro");
        keys.put("saldo", "saldo");

        return keys;

    }

    public String isDataType(String key, String value){

        value= value.trim();




        if( key.equals("cargos") &&   value.indexOf(" ") > -1 )
        {

            if(value.contains("Total")){
                return "END";
            }

            value= value.trim();
            String[] datos = value.split(" ");
            value = datos[datos.length -1 ];


        }

        
        if( (key.equals("cargos") || key.equals("abonos") 
        || key.equals("saldo")) && value.indexOf(" ") < 0 ){

 
            String regex = "^\\d*\\.\\d+|\\d+\\.\\d*$";
            value= value.trim();
            value= value.replace("$", "").replace(",","" );
            
            if(value.matches(regex)){
                return value;
             }else {
                 return "";
             }

        }


        return value;
    }


    public JSONObject adjustJson(JSONObject json){

        System.out.println(json.toJSONString());

        

        if(json.containsKey("fecha")){
            
            String fecha = (String) json.get("fecha");

            if(mes == null){
                for(Integer i=0; i< this.blocks.size(); i++){
                    this.addBlock((JSONObject) this.blocks.get(i));
                }
            }

            if(fecha!= null &&  mes != null){

                if(!fecha.equals("") && fecha.length() >= 2 && !fecha.contains(" ")){
                    String mesText = MESES[mes].toUpperCase().substring(0,3); 
                    fecha = fecha + "/" + mesText  + "/" + this.anio;
                    fecha = Util.replaceMonth(fecha);

                    json.put("fecha", fecha);
                }else if(!fecha.equals("") && fecha.indexOf(" ") > -1){
                    String[] datos = fecha.split(" ");
                    fecha= datos[0];
                    String mesText = MESES[mes].toUpperCase().substring(0,3); 
                    fecha = fecha + "/" + mesText  + "/" + this.anio;
                    fecha = Util.replaceMonth(fecha);
                    json.put("fecha", fecha);


                }
            }
        }
        if(json.containsKey("saldo")){

            //Es para cuando el salgo tiene muchos numeros

            String saldo = (String) json.get("saldo");
            saldo= saldo.trim();

            if(saldo.indexOf(" ") > -1){
                saldo= saldo.replace("$", "").replace(",","" );
                String[] datos = saldo.split(" ");

                json.put("saldo", convertNumber(datos[1]));
                json.put("deposito", convertNumber(datos[0]));


            }

        }

        if(json.containsKey("retiro")){

            //Es para cuando el salgo tiene muchos numeros
            String retiro = (String) json.get("retiro");
            retiro= retiro.trim();

            if(retiro.indexOf(" ") > -1 ){
                String[] datos = retiro.split(" ");

                json.put("retiro", convertNumber(datos[1]));

            }else {
                json.put("retiro", convertNumber(retiro));


            }

        }
        
        if(json.containsKey("deposito") && json.containsKey("retiro")){
            String retiro = (String) json.get("retiro");
            String deposito = (String) json.get("deposito");

            if(!retiro.equals("") && !deposito.equals("")){
                return null;

            }
            
        }

        

        
        return json;
    }


    public Boolean isValidFormatToContinue(JSONObject item){

        System.out.println("---JSON " + item.toJSONString());


        String retiro = (String) item.get("retiro");

        if(retiro != null){
            if(retiro.equals("END")){
                return false;
            }
        }

        


        if(item.containsKey("fecha")){
            String fecha  = (String) item.get("fecha");
            fecha= fecha.trim();

            if(fecha== null){
                System.out.println("YA NO CONTINIA 1");
                return false;
            }else if(!fecha.equals("")){
                return true;
            } else if(fecha.equals("")){
                String concepto  = (String) item.get("concepto");


                if(concepto.contains("Total") || concepto.contains("Transaccional")){
                    return false;
                }


                if(concepto.equals("") && !this.previuosHasConcepto){
                    this.previuosHasConcepto= false;
                    return false;
                }else {
                    this.previuosHasConcepto= true;
                }
                    
               
                return true;
            }
        }  


        System.out.println("YA NO CONTINIA 3");

        return false;
     
    }


    public String convertNumber(String number){

        String regex = "^\\d*\\.\\d+|\\d+\\.\\d*$";
        number= number.trim();
        number= number.replace("$", "").replace(",","" );
        if(number.matches(regex)){
            return number;
         }else {
             return "";
         }

    }



    public Float getWidth(String key){

        //System.out.println("ENTROOO CON LA LLAVE: " + key);

     
        if(key.equals("concepto")){
            return 0.1f;

        }
        if(key.equals("cargos")){
            return 0.12f;

        }
        
        return 0.0f;

    }


    public Boolean isCenter(String key){

        if(key.equals("dia")){
            return false;
        }

        return true;
    }

}