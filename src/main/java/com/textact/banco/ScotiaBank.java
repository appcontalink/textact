package com.textact.banco;

import com.textact.banco.Banco;
import com.textact.resources.Util;

import java.util.Map;
import java.util.Calendar;
import java.util.HashMap;

import org.json.simple.JSONObject;

public class ScotiaBank extends Banco {

    private String anio = "";

    public static JSONObject banco() {
        JSONObject banco = new JSONObject();
        banco.put("value", "scotiabank");
        banco.put("label", "Scotiabank");
        banco.put("active", true);

        return banco;

    }

    public ScotiaBank(String jobId) {
        super(jobId);
        this.anio = this.recoverYear();

    }

    public void addBlock(JSONObject item) {
        String text = (String) item.get("text");

        if (text != null) {
            text = text.trim();

            if (this.anio == null) {

                if (Util.hasMonthWithGuion(text)) {

                    System.out.println(text);

                    String fechas[] = text.split("-");
                    if (fechas.length >= 2) {
                        try {

                            String anio = fechas[fechas.length - 1];
                            System.out.println(anio);
                            if (Util.isNumber(anio)) {
                                anio = "20" + anio;
                                System.out.println(anio);
                                save(anio);
                                this.anio = anio;

                            }
                        } catch (Exception e) {

                        }

                    }
                }
            }

        }
    }

    public String[] getTitles() {

        String[] title = { "fecha", "concepto", "origen / referencia", "depósito", "retiro", "saldo" };

        return title;
    }

    public String[] getTitlesHeader() {

        String[] title = { "fecha", "concepto", "origen / referencia", "depósito", "retiro", "saldo" };

        return title;
    }

    public String getTitleLeader() {
        return "concepto";
    }

    public Map<String, String> getKeys() {

        Map<String, String> keys = new HashMap<String, String>();

        keys.put("concepto", "concepto");
        keys.put("fecha", "fecha");
        keys.put("retiro", "retiro");
        keys.put("retiros", "retiro");
        keys.put("saldo", "saldo");
        keys.put("origen / referencia", "origen_referencia");
        keys.put("depósito", "deposito");

        return keys;

    }

    public String isDataType(String key, String value) {

        // System.out.println("KEY " + key + ": " + value);

        if (key.equals("depósito") || key.equals("retiro")) {

            // Cuando hay un deposito
            if (key.equals("depósito") && value.indexOf("$") > -1) {
                Integer indexPrecio = value.indexOf("$");
                value = value.substring(indexPrecio + 1);
            }

            String regex = "^\\d*\\.\\d+|\\d+\\.\\d*$";
            value = value.trim();
            value = value.replace("$", "").replace(",", "");
            if (value.matches(regex)) {
                return value;
            } else {
                return "";
            }

        } else if (key.equals("fecha")) {
            if (Util.isNumber(value)) {
                return "";
            } else {
                return value;
            }
        }

        return value;
    }

    public JSONObject adjustJson(JSONObject item) {

        // En caso de que fuera nulo
        if (this.anio == null) {
            int year = Calendar.getInstance().get(Calendar.YEAR);
            this.anio = String.valueOf(year);

        }

        // Caso scoatiabank
        if (item.containsKey("origen_referencia") && item.containsKey("concepto")) {

            String origenReferencia = (String) item.get("origen_referencia");
            String concepto = (String) item.get("concepto");

            if (!concepto.equals("") && !origenReferencia.equals("")) {
                if (concepto.contains(origenReferencia)) {
                    item.put("origen_referencia", "");
                }
            }

        }

        String saldo = (String) item.get("saldo");
        saldo = saldo.trim();
        if (saldo.indexOf(" ") > -1) {
            Integer space = saldo.indexOf(" ");
            String retiro = saldo.substring(0, space);
            retiro = retiro.replace("$", "").replace(",", "");
            item.put("retiro", retiro);
            saldo = saldo.substring(space + 1);
            saldo = saldo.replace("$", "").replace(",", "");
            item.put("saldo", saldo);

        } else {
            saldo = saldo.replace("$", "").replace(",", "");
            item.put("saldo", saldo);

        }

        String fecha = (String) item.get("fecha");

        if (!fecha.equals("")) {
            fecha = Util.replaceMonth(fecha);
            fecha = fecha.trim();
            String date[] = fecha.split(" ");
            if (date.length > 1) {
                item.put("fecha", date[0] + "/" + date[1] + "/" + this.anio);

            }
        }

        return item;

    }

    public Boolean isValidFormatToContinue(JSONObject item) {
        String fecha = (String) item.get("fecha");

        if (fecha == null) {
            return false;
        }
        fecha = fecha.trim();

        if (fecha.equals("")) {
            return true;
        } else if (fecha.contains("ENE") || fecha.contains("FEB")
                || fecha.contains("MAR") || fecha.contains("ABR") || fecha.contains("MAY")
                || fecha.contains("JUN") || fecha.contains("JUL") || fecha.contains("AGO")
                || fecha.contains("SEP") || fecha.contains("OCT") || fecha.contains("NOV")
                || fecha.contains("DIC")) {
            return true;
        }

        System.out.println("Ya no continua" + fecha);

        return false;
    }

}