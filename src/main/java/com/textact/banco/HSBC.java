package com.textact.banco;
import com.textact.banco.Banco;

import java.util.Map;
import java.util.HashMap;

import org.json.simple.JSONObject;
import com.textact.resources.Util;


public class HSBC  extends Banco {

    public static final Map<String, String> dictMesAnio = new HashMap<String, String>();

    private String mesAnio="";
    private String jobId="";


    public static JSONObject banco(){
        JSONObject banco = new JSONObject();
        banco.put("value", "hsbc");
        banco.put("label", "HSBC");
        banco.put("active", true);

        return banco;

    }

    public HSBC(String jobId){

        super(jobId);
        this.jobId= jobId;

        
    }

    public void addBlock(JSONObject item){

        String text = (String) item.get("text");
        if(text != null){
            text= text.trim().toLowerCase();

            if(text.contains("/")){

                String fechas[]  = text.split("/");
                if(fechas.length >= 2 && this.dictMesAnio.isEmpty()){
                    try {

                        System.out.println("SPLITTING !: " + fechas[1]);
                        System.out.println("SPLITTING 2: " + fechas[2]);

                        String mes = fechas[1];
                        String anio="";

                        if(fechas[2].contains(".")){
                            anio = "No Aceptado";

                        }else {
                            anio = fechas[2].trim().substring(0, 4);

                        }

                        if(Util.isNumber(anio) && Util.isNumber(mes)){
                            this.mesAnio = "/" + mes + "/" + anio;
                            this.dictMesAnio.put(this.jobId, this.mesAnio);
                            System.out.println("SPLITTING mesAnio: " + mesAnio);

                        }
                    } catch (Exception e){

                    }

                    
                }

            }

        }



        
    }


    public String[] getTitles(){
        String[] titles=  { "día", "descripción", "retiro/cargo",  "depósito/abono", "saldo"};

        return titles;
    }

    public String[] getTitlesHeader(){
        String[] titles=  { "fecha", "concepto", "retiros",  "depositos", "depósitos", "saldo"};

        return titles;
    }



    public String getTitleLeader(){
        return "descripción";
    }

    public Map<String, String> getKeys(){

        Map<String, String> keys= new HashMap<String, String>();
        keys.put("descripción", "concepto");
        keys.put("día", "fecha");
        keys.put("retiro/cargo", "retiro");
        keys.put("depósito/abono", "deposito");
        keys.put("saldo", "saldo");

        return keys;

    }


    public String isDataType(String key, String value){

        if(this.dictMesAnio.containsKey(this.jobId)){
            this.mesAnio = this.dictMesAnio.get(this.jobId);
        }


        //System.out.println("isDataType: "+ key +" : "+  value);


        if(key.equals("retiro/cargo")){
            if(value.indexOf("$") > 0){
                Integer precio = value.indexOf("$");
                value = value.substring(precio);
            }else {
                return "";
            }
        }


        if( key.equals("retiro/cargo") || key.equals("depósito/abono") || key.equals("saldo")){


            String regex = "^\\d*\\.\\d+|\\d+\\.\\d*$";
            value= value.trim();
            value= value.replace("$", "").replace(",","" ).replace(" ", "");
            if(value.matches(regex)){
                return value;
             }else {
                 return "";
             }

        }

        

        if(key.equals("día")){
            value=Util.getFirsttWord(value);
            if(!value.equals("")){
                if(Util.isNumber(value)){
                    value= value + this.mesAnio;
                }
            }
        }


        return value;
    }


    public JSONObject adjustJson(JSONObject json){

        //System.out.println("adjustJson:" + json.toJSONString());


        return json;
    }


    public Boolean isValidFormatToContinue(JSONObject item){

        //System.out.println("isValidFormatToContinue:" + item.toJSONString());

        String fecha = (String) item.get("fecha");

        if(fecha == null){
            return false;
        }
        
        fecha= fecha.trim();

        if(fecha.contains("Emitido")){
            return false;
        }

        if(fecha.contains("CoDi")){
            return false;
        }



        return true;
    }



}