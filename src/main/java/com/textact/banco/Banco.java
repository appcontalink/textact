package com.textact.banco;

import java.util.Map;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.json.simple.JSONObject;
import org.json.simple.JSONArray;

import com.textact.db.EntityDatabase;
import com.textact.db.Query;
import com.textact.pdf.BoundingBoxPDF;

public class Banco {

    public JSONArray blocks;
    public String jobId;

    public Banco(String jobId) {
        this.jobId = jobId;
    }

    public String[] getTitles() {
        return new String[0];
    }

    public String getTitleLeader() {
        return "";
    }

    public void addBlock(JSONObject item) {

    }

    public Map<String, String> getKeys() {

        return new HashMap<String, String>();

    }

    public String isDataType(String key, String value) {
        return value;
    }

    public JSONObject adjustJson(JSONObject json) {
        return json;

    }

    // Es para continuar cuando llega al final
    public Boolean isValidFormatToContinue(JSONObject item) {
        return true;
    }

    public String[] getTitlesHeader() {
        return new String[0];
    }

    // Rango para buscar la medidas del top
    public Double getBottomHeader() {
        return 0.004;

    }

    public Double getTopHeader() {
        return 0.004;

    }

    public Float getWidth(String key) {
        return 0.0f;

    }

    public Boolean isCenter(String key) {
        return false;
    }

    public Float beginTableOne(List<BoundingBoxPDF> headers, String valorLeader) {
        Float topHeader = 1.0f;

        for (BoundingBoxPDF box : headers) {
            String text = box.getText();
            text = text.toLowerCase().trim();

            if (text.equals(valorLeader)) {
                if (topHeader > box.getTop()) {
                    topHeader = box.getTop();
                }
            }

        }

        return topHeader;

    }

    public Float beginTableSecond(JSONArray blocksJson) {
        Float topHeader = 100.0f;
        return topHeader;

    }

    public void save(String year) {

        System.out.println("Se esta guardando");

        EntityDatabase db = new EntityDatabase();

        try {
            db.openConnection();

            db.setTable("account_status_pdf");

            JSONArray items = db.get(Query.eq("job_id", this.jobId), null, null, null);

            if(items.size() > 0) {
                JSONObject item = (JSONObject) items.get(0);

                JSONObject updatedItem = new JSONObject();

                String currentYear=(String) item.get("year");

                if(currentYear == null){
                    updatedItem.put("year", year);
                    updatedItem.put("id", item.get("id"));
                    updatedItem = db.update(updatedItem);
                }
                
               

            }
            

            db.closeConnection();

        } catch (Exception e) {
            System.out.println(e.getMessage());

        }

    }

    public String recoverYear() {

        EntityDatabase db = new EntityDatabase();

        try {
            db.openConnection();
            db.setTable("account_status_pdf");

            JSONArray items = db.get(Query.eq("job_id", this.jobId), null, null, null);

            if(items.size() > 0) {
                JSONObject item = (JSONObject) items.get(0);
                String year = (String) item.get("year");
                return year;
            }
            
            db.closeConnection();

        } catch (Exception e) {

            System.out.println(e.getMessage());

        }



        //return String.valueOf(Calendar.getInstance().get(Calendar.YEAR));

        return null;

    }

}