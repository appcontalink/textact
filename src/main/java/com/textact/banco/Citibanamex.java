package com.textact.banco;

import com.textact.banco.Banco;

import java.util.Map;
import java.util.Calendar;
import java.util.HashMap;

import org.json.simple.JSONObject;
import com.textact.resources.Util;

public class Citibanamex extends Banco {

    private String anio = null;
    private String jobId = "";

    public static JSONObject banco() {
        JSONObject banco = new JSONObject();
        banco.put("value", "citibanamex");
        banco.put("label", "Citibanamex");
        banco.put("active", true);

        return banco;

    }

    public Citibanamex(String jobId) {
        super(jobId);
        this.jobId = jobId;
        this.anio = this.recoverYear();

    }

    public void addBlock(JSONObject item) {
        String text = (String) item.get("text");
        if (text != null) {
            text = text.trim().toLowerCase();

            if (this.anio == null) {

                if (text.contains("/")) {

                    String fechas[] = text.split("/");
                    if (fechas.length >= 2) {
                        try {

                            System.out.println("SPLITTING 1: " + fechas[1]);
                            System.out.println("SPLITTING 2: " + fechas[2]);

                            String mes = fechas[1];
                            String anio = fechas[2].trim().substring(0, 4);
                            if (Util.isNumber(anio) && Util.isMonth(mes)) {

                                this.anio = anio;
                                save(anio);

                            }
                        } catch (Exception e) {

                        }

                    }
                } else if (Util.containsYear(text) > -1) {
                    Integer index = Util.containsYear(text);
                    String anio = text.substring(index);
                    anio = anio.trim();
                    String mes = Util.containsMonth(text);

                    if (Util.isNumber(anio)) {

                        this.anio = anio;
                        save(anio);
                    }

                }
            }

        }

    }

    public String[] getTitles() {
        String[] titles = { "fecha", "concepto", "retiros", "depositos", "depósitos", "saldo" };

        return titles;
    }

    public String[] getTitlesHeader() {
        String[] titles = { "fecha", "concepto", "retiros", "depositos", "depósitos", "saldo" };

        return titles;
    }

    public String getTitleLeader() {
        return "concepto";
    }

    public Map<String, String> getKeys() {

        Map<String, String> keys = new HashMap<String, String>();
        keys.put("concepto", "concepto");
        keys.put("fecha", "fecha");
        keys.put("retiros", "retiro");
        keys.put("depósitos", "deposito");
        keys.put("saldo", "saldo");
        keys.put("depositos", "deposito");

        return keys;

    }

    public String isDataType(String key, String value) {

        System.out.println("key: " + key + ", value: " + value);

        if (key.equals("depósitos") || key.equals("deposito") || key.equals("depositos") || key.equals("retiros")
                || key.equals("saldo")) {

            String regex = "^\\d*\\.\\d+|\\d+\\.\\d*$";
            value = value.trim();
            value = value.replace("$", "").replace(",", "");
            if (value.matches(regex)) {
                return value;
            } else {
                return "";
            }

        }

        if (key.equals("fecha")) {
            if (Util.numberWords(value) == 2) {
                return value;
            } else {
                return "";
            }
        }

        return value;
    }

    public JSONObject adjustJson(JSONObject json) {

        // En caso de que fuera nulo
        if (this.anio == null) {
            int year = Calendar.getInstance().get(Calendar.YEAR);
            this.anio = String.valueOf(year);

        }

        String fecha = (String) json.get("fecha");
        if (!fecha.equals("")) {
            String day = Util.getFirsttWord(fecha);
            String month = Util.getLastWord(fecha);
            fecha = day + "/" + month + "/" + this.anio;
            fecha = Util.replaceMonth(fecha);
            json.put("fecha", fecha);
        }

        System.out.println("JSON: " + json.toJSONString());

        return json;
    }

    public Boolean isValidFormatToContinue(JSONObject item) {
        String fecha = (String) item.get("fecha");
        String concepto = (String) item.get("concepto");

        if (concepto != null) {
            concepto = concepto.trim();
            if (concepto.contains("Saldo mínimo requerido para evitar la comisión")) {
                return false;
            }
        }

        if (fecha == null) {
            return false;
        }
        fecha = fecha.trim();

        if (fecha.equals("")) {
            return true;
        } else if (fecha.contains("ENE") || fecha.contains("FEB")
                || fecha.contains("MAR") || fecha.contains("ABR") || fecha.contains("MAY")
                || fecha.contains("JUN") || fecha.contains("JUL") || fecha.contains("AGO")
                || fecha.contains("SEP") || fecha.contains("OCT") || fecha.contains("NOV")
                || fecha.contains("DIC")) {

            return true;
        }

        System.out.println("No puede continuar: " + fecha);

        return false;
    }

    public Float getWidth(String key) {

        if (key.equals("retiros")) {
            return 0.02f;

        }

        return 0.0f;

    }

}