package com.textact.banco;

import com.textact.banco.Banco;

import java.util.Map;
import java.util.HashMap;

import org.json.simple.JSONObject;
import org.json.simple.JSONArray;

import java.util.Calendar;

import com.textact.resources.Util;

public class Bancomer extends Banco {

    String descripcion = "descripción";

    Boolean oper_liq = false;
    private String anio = null;

    public static JSONObject banco() {
        JSONObject banco = new JSONObject();
        banco.put("value", "bancomer");
        banco.put("label", "Bancomer");
        banco.put("active", true);

        return banco;

    }

    public Bancomer(String jobId) {

        super(jobId);
        this.anio = this.recoverYear();

    }

    public void addBlock(JSONObject item) {

        String text = (String) item.get("text");

        if (text != null) {
            text = text.trim().toLowerCase();
            // System.out.println("TEXTO BANCOMER" + text);

            if (text.equals("oper liq")) {
                oper_liq = true;
            }

            if (text.equals("cod. descripción")) {
                this.descripcion = "cod. descripción";
            }

            if (text.equals("descripcion")) {
                this.descripcion = "descripcion";
            }

            if (this.anio == null) {

                if (text.contains("/")) {

                    String fechas[] = text.split("/");
                    if (fechas.length >= 2) {
                        try {

                            System.out.println("SPLITTING 1: " + fechas[1]);
                            System.out.println("SPLITTING 2: " + fechas[2]);

                            String anio = fechas[2].trim().substring(0, 4);
                            System.out.println(anio);
                            if (Util.isNumber(anio)) {

                                save(anio);
                                this.anio = anio;

                            }
                        } catch (Exception e) {

                        }

                    }
                }
            }

        }

    }

    public String[] getTitles() {

        if (oper_liq) {
            String[] titles = { "oper liq", "cod.", descripcion, "cargos", "abonos", "operación", "operacion",
                    "liquidación" };
            return titles;

        } else {
            String[] titles = { "oper", "liq", "cod.", descripcion, "cargos", "abonos", "operación", "operacion",
                    "liquidación" };
            return titles;

        }

    }

    public String[] getTitlesHeader() {
        String[] titles = { "oper", "liq", "cod. descripción", "cargos", "abonos", "operación", "liquidación" };

        return titles;
    }

    public String getTitleLeader() {

        if (oper_liq) {
            return "oper liq";
        }

        return "oper";
    }

    public Map<String, String> getKeys() {

        Map<String, String> keys = new HashMap<String, String>();

        keys.put(descripcion, "concepto");
        keys.put("oper", "fecha");
        keys.put("liq", "liq");
        keys.put("operación", "saldo");
        keys.put("referencia", "referencia");
        keys.put("abonos", "deposito");
        keys.put("cargos", "retiro");
        keys.put("cod.", "cod");
        keys.put("oper liq", "fecha");
        keys.put("descripcion", "concepto");
        keys.put("operacion", "saldo");

        return keys;

    }

    public String isDataType(String key, String value) {

        // System.out.println(key + ": " + value);

        if (key.equals("cargos")) {
            value = Util.getLastWord(value);
        }

        value = value.trim();

        if (key.equals("operación")) {
            if (!value.equals("")) {
                if (value.indexOf(" ") > -1) {
                    value = value.trim();
                    Integer space = value.indexOf(" ");
                    value = value.substring(0, space);
                }
            }
        }

        if (key.equals("abonos")) {
            if (!value.equals("")) {
                if (value.indexOf(" ") > -1) {
                    value = value.trim();
                    Integer space = value.indexOf(" ");
                    value = value.substring(0, space);
                }
            }
        }

        if (key.equals("abonos")
                || key.equals("liquidación") || key.equals("operación") || key.equals("cargos")) {

            String regex = "^\\d*\\.\\d+|\\d+\\.\\d*$";
            value = value.trim();
            value = value.replace("$", "").replace(",", "");
            if (value.matches(regex)) {
                return value;
            } else {
                return value;
            }

        } else if (key.equals("oper") && value.indexOf(" ") > -1) {
            value = value.trim();
            if (!value.equals("")) {
                Integer space = value.indexOf(" ");
                return value.substring(0, space);

            }
        } else if (key.equals("oper liq") && value.indexOf(" ") > -1) {
            value = value.trim();
            if (!value.equals("")) {
                Integer space = value.indexOf(" ");
                return value.substring(0, space);

            }
        }

        return value;
    }

    public JSONObject adjustJson(JSONObject json) {


        //En caso de que fuera nulo
        if (this.anio == null) {
            int year = Calendar.getInstance().get(Calendar.YEAR);
            this.anio = String.valueOf(year);

        }



        if (json.containsKey("liq")) {
            String text = (String) json.get("liq");
            if (text.indexOf(" ") > -1) {
                Integer ultimo = text.indexOf(" ");
                String concepto = text.substring(ultimo, text.length());
                concepto = concepto.trim();
                json.put("concepto", concepto);
            }
        }

        if (!json.containsKey("concepto")) {
            json.put("concepto", "");
        }

        if (json.containsKey("cod")) {
            String concepto = (String) json.get("concepto");
            String cod = (String) json.get("cod");

            json.put("concepto", (cod + " " + concepto).trim());
        }

        if (json.containsKey("retiro")) {
            String retiro = (String) json.get("retiro");

            if (isNumber(retiro)) {
                json.put("retiro", convertNumber(retiro));

            } else {
                String concepto = (String) json.get("concepto");
                json.put("concepto", (concepto + " " + retiro).trim());
                json.put("retiro", "");

            }
        }

        if (json.containsKey("deposito")) {
            String deposito = (String) json.get("deposito");
            if (deposito.indexOf(".") > -1) {
                json.put("deposito", deposito);

            } else {
                json.put("deposito", "");
            }

        }

        if (!json.containsKey("saldo")) {
            json.put("saldo", "");
        }

        String saldo = (String) json.get("saldo");
        if (saldo == null) {
            json.put("saldo", "");
        }

        String fecha = (String) json.get("fecha");

        if (!fecha.equals("")) {
            fecha = fecha + "/"+  anio;
            fecha = Util.replaceMonth(fecha);
            json.put("fecha", fecha);
        }

        System.out.println(json.toJSONString());

        return json;
    }

    public Boolean isValidFormatToContinue(JSONObject item) {

        String fecha = (String) item.get("fecha");
        String concepto = (String) item.get("concepto");
        String retiro = (String) item.get("retiro");

        if (concepto != null) {
            if (concepto.indexOf("Estimado") > -1) {
                return false;
            } else if (concepto.indexOf("Su Estado de Cuenta") > -1) {
                return false;

            }
        }

        if (retiro != null) {
            if (retiro.indexOf("Estimado") > -1) {

                return false;
            }
        }

        if (fecha == null) {
            return false;
        }
        fecha = fecha.trim();

        if (fecha.equals("")) {
            return true;
        } else if (fecha.contains("ENE") || fecha.contains("FEB")
                || fecha.contains("MAR") || fecha.contains("ABR") || fecha.contains("MAY")
                || fecha.contains("JUN") || fecha.contains("JUL") || fecha.contains("AGO")
                || fecha.contains("SEP") || fecha.contains("OCT") || fecha.contains("NOV")
                || fecha.contains("DIC")) {
            return true;
        }

        if (fecha.equals("") && concepto.equals("")) {

            return false;
        }

        return false;
    }

    public String convertNumber(String number) {

        String regex = "^\\d*\\.\\d+|\\d+\\.\\d*$";
        number = number.trim();
        number = number.replace("$", "").replace(",", "");
        if (number.matches(regex)) {
            return number;
        } else {
            return "";
        }

    }

    public Boolean isNumber(String number) {

        String regex = "^\\d*\\.\\d+|\\d+\\.\\d*$";
        number = number.trim();
        number = number.replace("$", "").replace(",", "");
        if (number.matches(regex)) {
            return true;
        } else {
            return false;
        }

    }

    public Float beginTableSecond(JSONArray texts) {
        Float topHeader = 1.0f;
        System.out.println("BUSCANDO LA SEGUNDA OPCION");

        for (Integer i = 0; i < texts.size(); i++) {
            JSONObject textJson = (JSONObject) texts.get(i);
            JSONObject geometry = (JSONObject) textJson.get("geometry");
            JSONObject boundingBox = (JSONObject) geometry.get("bounding_box");
            Float top = Util.toFloat(boundingBox.get("top"));

            String text = (String) textJson.get("text");
            if (text != null) {
                System.out.println("BUSCANDO LA SEGUNDA OPCION 2");

                text = text.trim();
                text = text.toLowerCase();

                if (text.contains("no. de cliente")) {
                    if (topHeader > top) {
                        topHeader = top;
                    }
                }
            }

        }

        return topHeader;

    }

}