package com.textact.http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.Iterator;
import java.util.Map;
import java.net.URLEncoder;

import javax.net.ssl.SSLSocketFactory;

import javax.net.ssl.HostnameVerifier;

public class Connection {


	int connectTimeout = 0;
	int readTimeout = 0;
	SSLSocketFactory sslSocket;
	HostnameVerifier hostname;
	private final String boundary;

	public Connection() {
		boundary = "===" + System.currentTimeMillis() + "===";

	}

	public Response send(Request request) throws ConnectionException {

		Response response = new Response();
		java.net.HttpURLConnection httpCon = null;
		int status = 0;
		try {

			String url = request.getUrl();

			if (!request.getParam().isEmpty()) {

				String parametros = "?";

				Iterator it = request.getParam().entrySet().iterator();

				while (it.hasNext()) {
					Map.Entry pair = (Map.Entry) it.next();
					String value = (String ) pair.getValue();
					value = URLEncoder.encode(value, "UTF-8");
					parametros = parametros + pair.getKey() + "=" + value + "&";
					it.remove();
				}
				parametros = parametros.substring(0, parametros.length() - 1);

				url = url + parametros;

			}


			url = url.trim();


			URL endURL = new URL(url);
			httpCon = (java.net.HttpURLConnection) endURL.openConnection();

            httpCon.setRequestProperty("Accept-Charset", "UTF-8");
			httpCon.setDoOutput(true);
			httpCon.setDoInput(true);


			httpCon.setDoOutput(true);
			if (request.getContentType() != null)
				httpCon.setRequestProperty("Content-Type", request.getContentType());


			if (request.getSoapAction() != null)
				httpCon.setRequestProperty("SOAPAction", request.getSoapAction());

			httpCon.setRequestMethod(request.getMethod());

			if (connectTimeout > 0)
				httpCon.setConnectTimeout(connectTimeout);
			if (readTimeout > 0)
				httpCon.setReadTimeout(readTimeout);

			if (request.getxAuthToken() != null) {
				httpCon.setRequestProperty("x-auth-token", request.getxAuthToken());
			}

			if (request.getContentLength() != null) {
				httpCon.setRequestProperty("Content-Length", request.getContentLength());
			}

			
			if (request.getAuthorization() != null) {
				httpCon.setRequestProperty("Authorization", request.getAuthorization());
			}

			if (request.getXrefid() != null) {
				httpCon.setRequestProperty("x-refid", request.getXrefid());
			}
			
			if (request.getContentMD5() != null) {
				httpCon.setRequestProperty("content-md5",request.getContentMD5());
			}

			for (Map.Entry<String, String> pair : request.getHeaders().entrySet()) {
				String key = pair.getKey();
				String value= pair.getValue();
				httpCon.setRequestProperty(key, value);

    		}

			if (request.getBody() != null) {

				OutputStreamWriter outputStream = new OutputStreamWriter(httpCon.getOutputStream(), "UTF8");
				outputStream.write(request.getBody());
				outputStream.flush();

			}



			// Preparamos el response
			response = new Response();

			status = httpCon.getResponseCode();
			response.setStatus(status);

			InputStream inputStream = (InputStream) httpCon.getInputStream();

			if (inputStream != null) {

				StringBuffer sb = new StringBuffer();

				BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));

				String inputLine = "";

				while ((inputLine = br.readLine()) != null) {
					sb.append(inputLine);
				}

				response.setContentType(httpCon.getContentType());
				response.setBody(sb.toString());

			}

			httpCon.disconnect();
		} catch (SocketTimeoutException e) {

			
			httpCon.disconnect();
			throw new ConnectionException(e.getMessage(), status);

		} catch (IOException e) {

			httpCon.disconnect();
			throw new ConnectionException(e.getMessage(), status);

		}

		return response;

	}


	 
	/**
	 * @return the connectTimeout
	 */
	public int getConnectTimeout() {
		return connectTimeout;
	}

	/**
	 * @param connectTimeout
	 *            the connectTimeout to set
	 */
	public void setConnectTimeout(int connectTimeout) {
		this.connectTimeout = connectTimeout;
	}

	/**
	 * @return the readTimeout
	 */
	public int getReadTimeout() {
		return readTimeout;
	}

	/**
	 * @param readTimeout
	 *            the readTimeout to set
	 */
	public void setReadTimeout(int readTimeout) {
		this.readTimeout = readTimeout;
	}

	/**
	 * @return the sslSocket
	 */
	public SSLSocketFactory getSslSocket() {
		return sslSocket;
	}

	/**
	 * @param sslSocket
	 *            the sslSocket to set
	 */
	public void setSslSocket(SSLSocketFactory sslSocket) {
		this.sslSocket = sslSocket;
	}

	/**
	 * @return the hostname
	 */
	public HostnameVerifier getHostname() {
		return hostname;
	}

	/**
	 * @param hostname
	 *            the hostname to set
	 */
	public void setHostname(HostnameVerifier hostname) {
		this.hostname = hostname;
	}

}
