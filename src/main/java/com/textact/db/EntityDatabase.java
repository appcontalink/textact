package com.textact.db;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSetMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.textact.db.Column;
import com.textact.db.Util;


public class EntityDatabase {

	private String table;
	private Connection conn = null;
	List<Column> columns = new ArrayList<Column>();

	String port;
	String host;
	String database;
	String user;
	String password;
	String lybrary;
	String  rdbms;



	public EntityDatabase() {
		this.port = System.getenv("port");
        this.host = System.getenv("host");
        this.user = System.getenv("user");
        this.password = System.getenv("password");
        this.database = System.getenv("database");
	}

	public EntityDatabase( String port, String host, String database, String user, String password) {
		this.port = port;
		this.host = host;
		this.database = database;
		this.user = user;
		this.password = password;
	}

		public EntityDatabase(String lybrary, String rdbms, String port, String host, String database, String user, String password) {
		this.port = port;
		this.host = host;
		this.database = database;
		this.user = user;
		this.password = password;
		this.lybrary = lybrary;
		this.rdbms = rdbms;
	}

	public String getTable() {
		return table;
	}

	public void setTable(String table) {
		columns = new ArrayList<Column>();
		this.table = table;
	}

	public void openConnection() throws SQLException, ClassNotFoundException {

		if(lybrary == null ){
			lybrary = "org.postgresql.Driver";
			rdbms = "postgresql";
		}

		System.out.println(host);

		Class.forName(lybrary);
		conn = DriverManager.getConnection("jdbc:"+rdbms+"://" + host + ":" + port + "/" + database, user, password);
		conn.setAutoCommit(true);

	}

	public void closeConnection() throws SQLException {
		conn.close();
	}

	public JSONArray get(String _where, String _orderBy, String _page, String _size)
			throws ClassNotFoundException, SQLException, ParseException {

		JSONArray arrayJson = null;

		String sql = "SELECT <columns> FROM " + this.table;

		if (_where != null) {
			_where = _where.replace("\"", "'");
			_where = translateWhere(_where);
			sql = sql + " WHERE " + _where;
		}

		if (_orderBy != null) {
			sql = sql + " ORDER BY " + _orderBy;
		}

		if (_size != null) {
			if (Util.isNumber(_size)) {
				sql = sql + " LIMIT  " + _size;
				if (_page != null) {
					if (Util.isNumber(_page)) {
						Integer page = Integer.parseInt(_page) * Integer.parseInt(_size);
						sql = sql + " OFFSET  " + page;
					}
				}
			}
		}

		readColumns();
		String orderColumns = "";
		for (Column col : this.columns) {
			orderColumns = orderColumns + " " + Util.reservedWord(col.getName()) + " " + ",";

		}
		orderColumns = orderColumns.substring(0, orderColumns.length() - 1);

		sql = sql.replace("<columns>", orderColumns);

		System.out.println(sql);

		arrayJson = executeQuery(sql, this.table);

		// System.out.println(arrayJson.toJSONString());

		return arrayJson;

	}



	public JSONObject get(Long id) throws ClassNotFoundException, SQLException, ParseException {

		JSONArray arrayJson = null;

		String sql = "SELECT <columns> FROM " + this.table + " WHERE id = " + id ;

		readColumns();
		String orderColumns = "";
		for (Column col : this.columns) {
			orderColumns = orderColumns + " " + col.getName() + " " + ",";

		}
		orderColumns = orderColumns.substring(0, orderColumns.length() - 1);


		sql = sql.replace("<columns>", orderColumns);
		System.out.println(sql);

		arrayJson = executeQuery(sql, this.table);

		System.out.println(sql);

		if (arrayJson.size() > 0) {
			return (JSONObject) arrayJson.get(0);
		} else {
			return new JSONObject();
		}

	}


	public JSONObject getSize(String _where)
			throws ClassNotFoundException, SQLException, ParseException {

		JSONArray arrayJson = null;

		String sql = "SELECT COUNT(*) AS count FROM " + this.table ;

		if (_where != null) {
			_where = _where.replace("\"", "'");
			_where = translateWhere(_where);
			sql = sql + " WHERE " + _where;
		}


		//System.out.println(sql);
		this.columns.add(new Column("count", "Integer"));

		arrayJson = executeQuery(sql, this.table);
		if (arrayJson.size() > 0) {
			return (JSONObject) arrayJson.get(0);
		} else {
			return new JSONObject();
		}


	}

  public JSONObject save(JSONObject obj) throws ClassNotFoundException, SQLException, ParseException
    {

		if(obj.containsKey("id")){
			return update(obj);
		}

        String sql = "INSERT INTO " + this.table + " ( <keys> ) " + "VALUES ( <values> )";

        readColumns();
        

        String keys = "", values = "";
        for (Column col : this.columns)
        {

            if (obj.containsKey(col.getName()))
            {

                keys = keys + " " + Util.reservedWord(col.getName()) + " " + ",";
                if (col.getType().equals("json"))
                {

                    String valor = "";
                    Object objClass = obj.get(col.getName());
                    if(objClass instanceof JSONObject)
                    {
                        JSONObject data_json = (JSONObject) objClass;
                        if(data_json.containsKey("null")){
                            data_json.remove("null");
                        }
                        valor = data_json.toJSONString();
                    }
                    else if(objClass instanceof JSONArray)
                    {
                        JSONArray data_json = (JSONArray) objClass;
                        valor = data_json.toJSONString();
                    }

                    if(!valor.equals(""))
                    {
                        values = values + " '" + valor + "' " + ",";
                    }
                    else
                    {
                        values = values + " null " + ",";

                    }



                }
                else
                {

                    Object objClass = obj.get(col.getName());

                    if (objClass == null)
                    {
                        values = values + "null" + ",";

                    }
                    else if (objClass instanceof String)
                    {

                        String value = (String) objClass;
                        if (value.equals("null"))
                        {
                            values = values + "null" + ",";
                        }
                        else
                        {
                            values = values + "'" + value.replace("\'", "\'\'") + "'" + ",";
                        }

                    }
                    else if (objClass instanceof Long)
                    {
                        Long value = (Long) objClass;
                        values = values + "'" + String.valueOf(value) + "'" + ",";
                    }
                    else if (objClass instanceof Boolean)
                    {
                        Boolean value = (Boolean) objClass;
                        values = values + "'" + String.valueOf(value) + "'" + ",";
                    }
                    else if (objClass instanceof Double)
                    {
                        Double value = (Double) objClass;
                        values = values + "'" + String.valueOf(value) + "'" + ",";

                    }
                    else if (objClass instanceof Integer)
                    {
                        Integer value = (Integer) objClass;
                        values = values + "'" + String.valueOf(value) + "'" + ",";
                    }

                }

            }
        }

        keys = keys.substring(0, keys.length() -1);
        values = values.substring(0, values.length() -1);


		sql = sql.replace("<keys>", keys).replace("<values>", values);
		System.out.println(sql);

        sql = sql + " RETURNING <return_keys>";

        //System.out.println(sql);


        String return_keys = "";
        for (Column col : this.columns)
        {
            return_keys = return_keys + " " + Util.reservedWord(col.getName()) + " " + ",";
        }
        return_keys = return_keys.substring(0, return_keys.length() -1);
        sql = sql.replace("<return_keys>", return_keys) ;

        JSONObject json = executeWithReturn(sql, this.table);

        return json;


      
    }


     private JSONObject executeWithReturn(String sql, String table) throws SQLException, ParseException
    {

        Statement stmt = null;
        stmt = conn.createStatement();

        ResultSet rs = stmt.executeQuery(sql);

        JSONObject json = new JSONObject();

        if(rs.next()){
            for (Column col : this.columns)
            {
                putType(col, rs, json);

            }

            rs.close();
            stmt.close();

            return json;

        }else {

            throw new SQLException("Esta accion no se puede realizar");
        }





    }



	public JSONObject update(JSONObject obj) throws SQLException, ParseException, ClassNotFoundException {
		Integer id = toInteger(obj.get("id"));
		return update(obj, id);

	}

	public void update(JSONObject obj, String _where) throws SQLException, ParseException, ClassNotFoundException
    {


        JSONArray arrayJson = null;
        _where = translateWhere(_where);
		String sql = "UPDATE " + this.table + " SET  <update>  WHERE " + _where  ;
        
    
        readColumns();
        String update = Util.buildSets(this.columns, obj);
        sql = sql.replace("<update>", update);

        System.out.println(sql);

        executeUpdate(sql);

        
    }



	 public JSONObject update(JSONObject obj, int id) throws SQLException, ParseException, ClassNotFoundException
    {

        JSONArray arrayJson = null;
        String filtros=null;

        readColumns();

        String sql = "UPDATE " + this.table + " SET  <update>  WHERE id = " + id ;

        String update = Util.buildSets(this.columns, obj);

        sql = sql.replace("<update>", update);
        sql = sql + " RETURNING <return_keys>";


        String return_keys = "";
        for (Column col : this.columns)
        {
            return_keys = return_keys + " " + Util.reservedWord(col.getName()) + " " + ",";
        }
        
        return_keys = return_keys.substring(0, return_keys.length() -1);
        sql = sql.replace("<return_keys>", return_keys) ;

        System.out.println(sql);


        JSONObject json = executeWithReturn(sql, this.table);

        return json;

    }


	public void delete(int id) throws ClassNotFoundException, SQLException {
		String sql = "DELETE FROM " + this.table + " WHERE id = " + id + ";";
		executeUpdate(sql);

	}

	public void delete(int id, Long empresa_id) throws ClassNotFoundException, SQLException {
		String sql = "DELETE FROM " + this.table + " WHERE id = " + id + " AND empresa_id = " +empresa_id+";";
		executeUpdate(sql);

	}




	private void readColumns() throws SQLException
    {

        if (columns == null)
        {
            columns = new ArrayList<Column>();
        }

        if(columns.size() > 0)
        {
            return;
        }

        String schema=null;
        String tableSchema=null;


        if(table.contains(".")){
            String[] parts = table.split("\\.");
            schema= parts[0];
            tableSchema= parts[1];  
        } 


        ResultSet rsColumns = null;
        DatabaseMetaData meta = conn.getMetaData();
        if(schema != null)
            rsColumns = meta.getColumns(null, schema, tableSchema, null);
        else 
            rsColumns = meta.getColumns(null, "public", table, null);
        

        while (rsColumns.next())
        {

            Column col = new Column();
            col.setName(rsColumns.getString("COLUMN_NAME"));
            col.setType(rsColumns.getString("TYPE_NAME"));

            columns.add(col);

        }

        if(columns.size() <= 0)
        {

            throw new SQLException("The entity " + table + " doesn't exit");
        }

    }


    private JSONArray executeQuery(String sql, String table) throws SQLException, ParseException
    {

    
        Statement stmt = null;
        stmt = conn.createStatement();

        sql = "select row_to_json(words) AS data from  (" + sql +") words";
        ResultSet rs = stmt.executeQuery(sql);

        JSONArray arrayJson = new JSONArray();
        while (rs.next())
        {
            String data = rs.getString("data");
            JSONObject entityJson = (JSONObject) new JSONParser().parse(data);
            arrayJson.add(entityJson);

        }

        rs.close();
        stmt.close();

        return arrayJson;

    }



	public JSONArray executeQuery(String sql) throws SQLException, ParseException
    {

        Statement stmt = null;
        stmt = conn.createStatement();

        sql = "select row_to_json(words) AS data from  (" + sql +") words";
        ResultSet rs = stmt.executeQuery(sql);
		System.out.println(sql);

        JSONArray arrayJson = new JSONArray();
        while (rs.next())
        {
			String data = rs.getString("data");
			System.out.println("data " + data);

            JSONObject entityJson =(JSONObject) new JSONParser().parse(data);
            arrayJson.add(entityJson);

		}
		
		//System.out.println("TERMINO 1");


        rs.close();
        stmt.close();

        return arrayJson;

    }



	private void putType(Column col, ResultSet rs,JSONObject entityJson ) throws SQLException, ParseException {
		

		if (col.getType().equals("json")) {
			String data_json_string = rs.getString(col.getName());



			if (data_json_string != null) {
				data_json_string = data_json_string.replace("\\", "");

				Object obj = new JSONParser().parse(data_json_string);
				if(obj instanceof JSONObject ){
					JSONObject data_json = (JSONObject) new JSONParser().parse(data_json_string);
					entityJson.put(col.getName(), data_json);
				} else if(obj instanceof JSONArray){
					JSONArray data_json = (JSONArray) new JSONParser().parse(data_json_string);
					entityJson.put(col.getName(), data_json);
				}

			} else {
				JSONObject data_json = new JSONObject();
				entityJson.put(col.getName(), data_json);
			}

		} else if (col.getType().equals("serial") || col.getType().equals("int4")|| col.getType().equals("int8")) {
			entityJson.put(col.getName(), rs.getInt(col.getName()));
		} else if (col.getType().equals("numeric") || col.getType().equals("decimal") || col.getType().equals("float8")) {
			entityJson.put(col.getName(), rs.getDouble(col.getName()));
		} else if (col.getType().equals("boolean") || col.getType().equals("bool")) {
			entityJson.put(col.getName(), rs.getBoolean(col.getName()));
		} else if ( col.getType().equals("timestamp")) {
			String dateString= rs.getTimestamp(col.getName()) != null ? rs.getTimestamp(col.getName()).toString() : null;
			entityJson.put(col.getName(), dateString);
		} else if (col.getType().equals("date") ) {
			String dateString= rs.getString(col.getName()) != null ? rs.getString(col.getName()) : null;
			entityJson.put(col.getName(), dateString);
		} else {
			entityJson.put(col.getName(), rs.getString(col.getName()));
		}
	}


	private Integer getNextId() throws SQLException {
		Integer id= null;

		String sql = "SELECT nextval('"+ this.table +"_id_seq') AS id";
		Statement stmt = null;
		stmt = conn.createStatement();

		ResultSet rs = stmt.executeQuery(sql);
		while (rs.next()) {
			id = rs.getInt("id");
		}

		rs.close();
		stmt.close();

		return id;


	}

	public JSONObject executeStoredProcedure(String procedure, JSONObject json_request) throws SQLException, ParseException {


		String data = json_request.toJSONString().replace("\\n", "").replace("\\\"", "").replace("\'", "");

		String sql = "SELECT " + procedure + "('" + data + "') AS json_response";
		System.out.println(sql);
		Statement stmt = null;
		stmt = conn.createStatement();

		ResultSet rs = stmt.executeQuery(sql);
		JSONObject json_response = new JSONObject();

		while (rs.next()) {

			String data_json_string = rs.getString("json_response");
			data_json_string = data_json_string.replace("\\", "");
			json_response = (JSONObject) new JSONParser().parse(data_json_string);

		}

		rs.close();
		stmt.close();

		return json_response;

	}
	
	public String executeStoredProcedure(String procedure, String json_request) throws SQLException, ParseException {

		String sql = "SELECT " + procedure + "('" + json_request + "') AS json_response";
		//System.out.println(sql);

		Statement stmt = null;
		stmt = conn.createStatement();

		ResultSet rs = stmt.executeQuery(sql);
		String data_json_string=null;
		while (rs.next()) {

			data_json_string = rs.getString("json_response");
			//System.out.println(data_json_string);
			data_json_string = data_json_string.replace("\\", "");

		}


		rs.close();

		stmt.close();

		return data_json_string;

	}


	public void delete(String _where) throws ClassNotFoundException, SQLException
    {
		_where = translateWhere(_where);

        String sql = null;
        sql = "DELETE FROM " + this.table + " WHERE  " + _where;
        System.out.println(sql);


        executeUpdate(sql);

    }

	private void executeUpdate(String sql) throws SQLException {

		Statement stmt = null;
		stmt = conn.createStatement();
		//System.out.println(sql);
		stmt.executeUpdate(sql);

		stmt.close();

	}

	private String translateWhere(String _where) throws SQLException {

		String[] exprs = _where.split(" and | or | AND | OR");
		for (int e = 0; e < exprs.length; e++) {

			if (exprs[e].contains("eq(")) {

				String expresion = Util.find("eq\\([^\\)]+,[^\\)]+\\)", exprs[e]);
				String[] params = expresion.split(",");
				String expresionSQL = params[0].replace("eq(", "") + " = " + params[1].replace(")", "");
				_where = _where.replace(expresion, expresionSQL);

			} else if (exprs[e].contains("ne(")) {
				String expresion = Util.find("ne\\([^\\)]+,[^\\)]+\\)", exprs[e]);
				String[] params = expresion.split(",");
				String expresionSQL = params[0].replace("ne(", "") + " != " + params[1].replace(")", "");
				_where = _where.replace(expresion, expresionSQL);

			} else if (exprs[e].contains("gt(")) {
				String expresion = Util.find("gt\\([^\\)]+,[^\\)]+\\)", exprs[e]);
				String[] params = expresion.split(",");
				String expresionSQL = params[0].replace("gt(", "") + " > " + params[1].replace(")", "");
				_where = _where.replace(expresion, expresionSQL);

			} else if (exprs[e].contains("ge(")) {
				String expresion = Util.find("ge\\([^\\)]+,[^\\)]+\\)", exprs[e]);
				String[] params = expresion.split(",");
				String expresionSQL = params[0].replace("ge(", "") + " >= " + params[1].replace(")", "");
				_where = _where.replace(expresion, expresionSQL);

			} else if (exprs[e].contains("lt(")) {
				String expresion = Util.find("lt\\([^\\)]+,[^\\)]+\\)", exprs[e]);
				String[] params = expresion.split(",");
				String expresionSQL = params[0].replace("lt(", "") + " < " + params[1].replace(")", "");
				_where = _where.replace(expresion, expresionSQL);

			} else if (exprs[e].contains("le(")) {
				String expresion = Util.find("le\\([^\\)]+,[^\\)]+\\)", exprs[e]);
				String[] params = expresion.split(",");
				String expresionSQL = params[0].replace("le(", "") + " <= " + params[1].replace(")", "");
				_where = _where.replace(expresion, expresionSQL);

			} else if (exprs[e].contains("isNull(")) {
				String expresion = Util.find("isNull\\(\\w+\\)", exprs[e]);
				//System.out.println(expresion);
				String expresionSQL = expresion.replace("isNull(", "").replace(")", "") + " is null ";
				_where = _where.replace(expresion, expresionSQL);

			} else if (exprs[e].contains("isNotNull(")) {
				String expresion = Util.find("isNotNull\\(\\w+\\)", exprs[e]);
				//System.out.println(expresion);
				String expresionSQL = expresion.replace("isNotNull(", "").replace(")", "") + " is not null ";
				_where = _where.replace(expresion, expresionSQL);

			} else if (exprs[e].contains("in(")) {
				String expresion = Util.find("in\\([^\\)]+,[^\\)]+\\)", exprs[e]);
				String[] params = expresion.split(",");
				String expresionSQL = params[0].replace("in(", "") + " in (" + params[1].replace("|", ",").replace(")", "") +")";
				_where = _where.replace(expresion, expresionSQL);

			} else if (exprs[e].contains("like("))
            {
                String expresion = Util.find("like\\([^\\)]+,[^\\)]+\\)", exprs[e]);
                String[] params = expresion.split(",");
                params[0]= params[0].replace("like(", "");

                String expresionSQL = params[0] + " like " + params[1].replace(")", "");

                _where = _where.replace(expresion, expresionSQL);

            }
            else if (exprs[e].contains("iLike("))
            {

				String expresion = Util.find("iLike\\([^\\)]+,[^\\)]+\\)", exprs[e]);
                String[] params = expresion.split(",");
                params[0]= params[0].replace("iLike(", "");

                String expresionSQL = params[0] + " iLike " + params[1].replace(")", "");

                _where = _where.replace(expresion, expresionSQL);
            }else {
				throw new SQLException("La expresión " + exprs[e] + " no es valida");
			}

		}


		return _where;
	}


	public JSONArray executeSql(String sql) throws SQLException, ParseException
    {

        Statement stmt = null;
        stmt = conn.createStatement();

        sql = "select row_to_json(words) AS data from  (" + sql +") words";
        ResultSet rs = stmt.executeQuery(sql);
        System.out.println(sql);

        JSONArray arrayJson = new JSONArray();
        while (rs.next())
        {
            String data = rs.getString("data");
            JSONObject entityJson = (JSONObject) new JSONParser().parse(data);
            arrayJson.add(entityJson);

        }

        rs.close();
        stmt.close();

        return arrayJson;

    }

	private Integer toInteger(Object obj){

        if(obj instanceof Integer ){
            return (Integer) obj;
        } else if(obj instanceof Long){
            return ((Long) obj).intValue();
        } else if(obj instanceof String){
            return Integer.parseInt((String) obj);
        }

        return null;
    }


}
