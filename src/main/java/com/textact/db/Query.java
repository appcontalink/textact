package com.textact.db;


public class Query {

	public static String in(String column, String values[]){

		String result="";

		for(int i=0; i< values.length; i++ ){
			result= result +  values[i] + " | ";   
		}

		result= result.substring(0, result.length() -2);



		return "in(" + column + ", " + result + ")"; 

	}


	public static String in(String column, Long values[]){

		String result="";

		for(int i=0; i< values.length; i++ ){
			result= result +  values[i] + " | ";   
		}

		result= result.substring(0, result.length() -2);



		return "in(" + column + ", " + result + ")"; 

	}



	public static String in(String column, Integer values[]){

		String result="";

		for(int i=0; i< values.length; i++ ){
			result= result +  values[i] + " | ";   
		}

		result= result.substring(0, result.length() -2);



		return "in(" + column + ", " + result + ")"; 

	}




	public static String eq(String column, String value){

		return "eq(" + column + ", '" + value + "')"; 

	}

	public static String lt(String column, String value){

		return "lt(" + column + ", '" + value + "')"; 

	}


	public static String le(String column, String value){

		return "le(" + column + ", '" + value + "')"; 

	}



	public static String ge(String column, String value){

		return "ge(" + column + ", '" + value + "')"; 

	}


	public static String ne(String column, String value){

		return "ne(" + column + ", '" + value + "')"; 

	}

	public static String iLike(String column, String value){

		return "iLike(" + column + ", '" + value + "')"; 

	}


	public static String eq(String column, Long valueLong){
		String value = String.valueOf(valueLong);
		return "eq(" + column + ", '" + value + "')"; 

	}

	public static String ne(String column, Long valueLong){
		String value = String.valueOf(valueLong);
		return "ne(" + column + ", '" + value + "')"; 

	}

	public static String or(String... values){
		String result="";

		for(int i=0; i< values.length; i++ ){
			result= result +  values[i] + " OR ";   
		}

		result= result.substring(0, result.length() -4);


		return "(" + result + ")"; 

	}


	public static String and(String... values){
		String result="";

		for(int i=0; i< values.length; i++ ){
			result= result +  values[i] + " AND ";   
		}

		result= result.substring(0, result.length() -5);


		return "(" + result + ")"; 

	}


}