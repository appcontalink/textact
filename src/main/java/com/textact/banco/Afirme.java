package com.textact.banco;

import com.textact.banco.Banco;

import java.util.Map;
import java.util.Calendar;
import java.util.HashMap;

import org.json.simple.JSONObject;
import org.json.simple.JSONArray;

import com.textact.resources.Util;

public class Afirme extends Banco {

    public static String[] MESES = { " ene ", " feb ", " mar ", " abr ", " may ", " jun ", " jul ", " ago ", " sep ",
            " oct ", " nov ", " dic " };

    public String mesAnio = null;

    public Boolean previuosHasConcepto = true;
    public static final Map<String, Integer> mesesHash = new HashMap<String, Integer>();

    public static JSONObject banco() {
        JSONObject banco = new JSONObject();
        banco.put("value", "afirme");
        banco.put("label", "Afirme");
        banco.put("active", true);

        return banco;

    }

    public Afirme(String jobId) {
        super(jobId);
        this.mesAnio = this.recoverYear();

    }

    public void addBlock(JSONObject item) {
        String text = (String) item.get("text");

        if (text != null && mesAnio == null) {
            text = text.toLowerCase().trim();
            if (Util.hasMonthNumberWithGuion(text)) {
                String anio=text.substring(0,4);
                String mes  =text.substring(5,7);

                mesAnio  = mes + "/" + anio;
                save(mesAnio);


            }
        }
    }

    public String[] getTitles() {
        String[] titles = { "día", "descripción", "referencia", "depósitos", "retiros", "saldo" };

        return titles;
    }

    public String[] getTitlesHeader() {
        String[] titles = { "día", "descripción", "referencia", "depósitos", "retiros", "saldo" };

        return titles;
    }

    public String getTitleLeader() {
        return "descripción";
    }

    public Map<String, String> getKeys() {

        Map<String, String> keys = new HashMap<String, String>();

        keys.put("descripción", "concepto");
        keys.put("día", "fecha");
        keys.put("depósitos", "deposito");
        keys.put("retiros", "retiro");
        keys.put("saldo", "saldo");

        return keys;

    }

    public String isDataType(String key, String value) {

        value = value.trim();

        Integer numberPrice = Util.count(value, '$');

        if (key.equals("retiros") && numberPrice > 1) {

            value = value.trim();
            value = value.replace(" ", "");

            String[] datos = value.split("\\$");
            value = datos[1];

        }

        if (key.equals("retiros") && value.indexOf(" ") > -1) {

            value = value.trim();
            String[] datos = value.split(" ");
            value = datos[datos.length - 1];

        }

        if ((key.equals("depositos") || key.equals("retiros")
                || key.equals("saldo")) && value.indexOf(" ") < 0) {

            String regex = "^\\d*\\.\\d+|\\d+\\.\\d*$";
            value = value.trim();
            value = value.replace("$", "").replace(",", "");

            if (value.matches(regex)) {

                return value;
            } else {
                return "";
            }

        }

        return value;
    }

    public JSONObject adjustJson(JSONObject json) {


        // System.out.println("INICIO --- "+json.toJSONString());

        if (json.containsKey("fecha")) {

            String fecha = (String) json.get("fecha");

       

            if (fecha != null && mesAnio != null) {

                System.out.println("FECHA: " + fecha);

                if (!fecha.equals("") && fecha.length() >= 2 && !fecha.contains(" ")) {
                    System.out.println("FECHA: 1");

                    fecha = fecha + "/" +mesAnio;
                    fecha = Util.replaceMonth(fecha);
                    json.put("fecha", fecha);
                    System.out.println("FINAL: " + fecha);

                } else if (!fecha.equals("") && fecha.indexOf(" ") > -1) {
                    System.out.println("FECHA: 2");

                    String[] datos = fecha.split(" ");
                    fecha = datos[0];
                    fecha = fecha + "/" + mesAnio;
                    fecha = Util.replaceMonth(fecha);
                    json.put("fecha", fecha);

                }
            }
        }
        if (json.containsKey("saldo")) {

            // Es para cuando el salgo tiene muchos numeros

            String saldo = (String) json.get("saldo");
            saldo = saldo.trim();
            saldo = saldo.replace("$ ", "").replace("$", "").replace(",", "");

            if (saldo.indexOf(" ") > -1) {
                String[] datos = saldo.split(" ");
                json.put("saldo", convertNumber(datos[1]));
                json.put("deposito", convertNumber(datos[0]));

            } else {
                json.put("saldo", convertNumber(saldo));

            }

        }

        if (json.containsKey("retiro")) {

            // Es para cuando el salgo tiene muchos numeros
            String retiro = (String) json.get("retiro");
            retiro = retiro.trim();

            if (retiro.indexOf(" ") > -1) {
                String[] datos = retiro.split(" ");

                json.put("retiro", convertNumber(datos[1]));

            } else {
                json.put("retiro", convertNumber(retiro));

            }

        }

        if (json.containsKey("deposito")) {

            // Es para cuando el salgo tiene muchos numeros
            String deposito = (String) json.get("deposito");
            deposito = deposito.trim();
            deposito = deposito.replace("$ ", "").replace("$", "").replace(",", "");

            json.put("deposito", convertNumber(deposito));
        }

        if (json.containsKey("deposito") && json.containsKey("retiro")) {
            String retiro = (String) json.get("retiro");
            String deposito = (String) json.get("deposito");

            if (!retiro.equals("") && !deposito.equals("")) {
                return null;

            }

        }

        // System.out.println("FIN--" + json.toJSONString());

        return json;
    }

    public Boolean isValidFormatToContinue(JSONObject item) {

        // System.out.println("---JSON " + item.toJSONString());

        if (item.containsKey("fecha")) {
            String fecha = (String) item.get("fecha");
            fecha = fecha.trim();

            if (fecha == null) {
                return false;
            } else if (!fecha.equals("")) {
                String concepto = (String) item.get("concepto");

                if (fecha.contains("Método de Pago: PUE")) {
                    return false;
                } else if (fecha.contains("EIPAB")) {
                    return false;
                } else if (fecha.contains("IPAB")) {
                    return false;
                }

                return true;
            } else if (fecha.equals("")) {
                String concepto = (String) item.get("concepto");

                if (concepto.contains("Total") || concepto.contains("Transaccional")
                        || concepto.contains("Impuesto:002")) {
                    return false;
                }

                if (concepto.equals("") && !this.previuosHasConcepto) {
                    this.previuosHasConcepto = false;

                    return false;
                } else {
                    this.previuosHasConcepto = true;
                }

                return true;
            }
        }

        return false;

    }

    public String convertNumber(String number) {

        String regex = "^\\d*\\.\\d+|\\d+\\.\\d*$";
        number = number.trim();
        number = number.replace("$", "").replace(",", "");
        if (number.matches(regex)) {
            return number;
        } else {
            return "";
        }

    }

}