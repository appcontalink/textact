package com.textact.banco;

import com.textact.banco.Banco;


import java.util.Map;
import java.util.HashMap;


import org.json.simple.JSONObject;
import com.textact.resources.Util;

public class Santander extends Banco {

    String fecha="fecha";


    public static JSONObject banco(){
        JSONObject banco = new JSONObject();
        banco.put("value", "santander");
        banco.put("label", "Santander");
        banco.put("active", true);

        return banco;

    }

    public Santander(String jobId){

        super(jobId);

    }

    public void addBlock(JSONObject item){
        
        String text = (String) item.get("text");
        if(text != null){
            text= text.trim().toLowerCase();

            if(text.equals("f e c h a folio descripcion")){
                fecha="f e c h a folio descripcion";
            } else if(text.equals("fecha folio descripcion")){
                fecha="fecha folio descripcion";
            } 

            //System.out.println("SANTANDER: " +  text);

          
        }
    }


    public String[] getTitles(){

        if(fecha.equals("f e c h a folio descripcion")){
           // System.out.println("REGRESO ESTO");

            String[] titles= {fecha,  "depositos", "retiros",  "saldo"};
            return titles;

        } else if(fecha.equals("fecha folio descripcion")){
            // System.out.println("REGRESO ESTO");
 
             String[] titles= {fecha,  "depositos", "retiros",  "saldo"};
             return titles;
 
         }else {

            String[] titles= { "f e ch a", "f e c h" ,"fech a" , "f e c h a", "fecha", "folio", "descripcion", "deposito",  "depositos", "retiro", "retiros",  "saldo"};

            return titles;
        }


    }



    public String[] getTitlesHeader(){

        String[] title= { "f e ch a", "f e c h" ,"fech a", "fecha", "folio", "descripcion", "depositos", "deposito", "retiros", "retiro", "saldo"};

        return title;
    }

    
    public String getTitleLeader(){

        if(fecha.equals("f e c h a folio descripcion")){
            return fecha;

        }else if(fecha.equals("fecha folio descripcion")){
            return fecha;

        }else if(fecha.equals("fecha folio descripcion")){
            return fecha;

        }else {
            return "folio";
        }
    }


    public Map<String, String> getKeys(){

        Map<String, String> keys= new HashMap<String, String>();
 
        keys.put("descripcion", "concepto");
        keys.put("fecha", "fecha");
        keys.put("fech a", "fecha");
        keys.put("f e c h", "fecha");
        keys.put("f", "fecha");

        keys.put("f e ch a", "fecha");

        keys.put("f e c h a", "fecha");
        keys.put("retiro", "retiro");
        keys.put("retiros", "retiro");
        keys.put("saldo", "saldo");
        keys.put("deposito", "deposito");
        keys.put("depositos", "deposito");

        keys.put("folio", "folio");
        keys.put(fecha, "fecha");

        return keys;

    }

    public String isDataType(String key, String value){

       System.out.println("KEY " + key + ": " + value);

       if(key.equals("depositos")){
           value = Util.getLastWord(value);

           if(value.indexOf(".") == (value.length() -1)){
               return "";
           }
           
       }

      // System.out.println("VALUE " + key + ": " + value);

           
        if(key.equals("depositos") || key.equals("retiros")  
        || key.equals("deposito") || key.equals("retiro")|| key.equals("saldo")){


            String regex = "^\\d*\\.\\d+|\\d+\\.\\d*$";
            value= value.trim();
            value= value.replace("$", "").replace(",","" );
            if(value.matches(regex)){
                return value;
             }else {
                 return "";
             }

        }


        return value;
    }


    public JSONObject adjustJson(JSONObject item){

       // System.out.println("ITEM----  " + item.toJSONString());

         if(item.containsKey("fecha")){

            String texto = (String) item.get("fecha");

            if(!texto.equals("")){
                texto= texto.trim();

                if(texto.indexOf(" ") > -1){

                    String[] texts = texto.split(" ");

                    item.put("folio",  texts[1]);
                    item.put("fecha", Util.changesZero(texts[0]));

                } else {
                    item.put("fecha", Util.changesZero(texto));
                }

            } 

        }
        //System.out.println("ITEM FIN----  " + item.toJSONString());


        return item;


    }


    public Boolean isValidFormatToContinue(JSONObject item){

        String folio = (String) item.get("folio");
        String fecha = (String) item.get("fecha");
        String deposito = (String) item.get("deposito");
        deposito= deposito == null ? "": deposito.trim();
        //System.out.println("entroo " +  item.toJSONString());

        String retiro = (String) item.get("retiro");
        retiro= retiro == null ? "": retiro.trim();

        if(folio != null && fecha == null){

            if(folio.contains("CERTIFICACION")){
                return false;

            } else if(folio.contains("EXPEDICION")){
                return false;

            }else if(Util.hasMonthWithGuion(folio)){
                fecha = Util.getFirsttWord(folio);
                item.put("fecha", fecha);
            }else {
                fecha="";

                item.put("fecha", fecha);

            }
        }

        if(fecha == null){

            return false;
        }

        item = updateJson(item);
        //System.out.println("actualizado " +  item.toJSONString());

        fecha = (String) item.get("fecha");
        String concepto = (String) item.get("concepto");



        if(concepto != null){

            concepto= concepto.trim();

            if(concepto.equals("TOTAL") && (deposito.equals("0.00") || retiro.equals("0.00"))){

                return true;
                
            }else if(concepto.equals("TOTAL") && (!deposito.equals("0.00") || !retiro.equals("0.00"))){
                //System.out.println("termino 1");

                return false;
                
            } 
        }

        if(fecha == null){
            //System.out.println("termino 2");

            return false;
        }
        fecha= fecha.trim();

        if(fecha.equals("")){

            return true;
        } else if(Util.hasMonth(fecha)){
            return true;
        }

        //System.out.println("termino 3");

        return false;
    }


    //Casi especial
    private JSONObject updateJson(JSONObject item){
        String fecha = (String) item.get("fecha");
        fecha= fecha.trim();

        //ESTO ES UN CASO ESPECIAL
        if(Util.hasMonthWithGuion(fecha) && this.fecha.equals("f e c h a folio descripcion")){
            if(fecha.indexOf(" ") > -1){
                String[] datos = fecha.split(" ");
                String fechatemp = datos[0];
                String folio = datos[1];

                fecha= fecha.replace(folio, "");
                fecha= fecha.replace(fechatemp, "");

                item.put("fecha", fechatemp );
                item.put("folio", folio );

                item.put("concepto", fecha  );
                return item;


            }else {
                item.put("concepto", fecha  );
                item.put("fecha", "" );
                return item;


            }

        } else if(Util.hasMonthWithGuion(fecha) && this.fecha.equals("fecha folio descripcion")){
            if(fecha.indexOf(" ") > -1){
                String[] datos = fecha.split(" ");
                String fechatemp = datos[0];
                String folio = datos[1];

                fecha= fecha.replace(folio, "");
                fecha= fecha.replace(fechatemp, "");

                item.put("fecha", fechatemp );
                item.put("folio", folio );

                item.put("concepto", fecha  );
                return item;


            }else {

                item.put("concepto", fecha  );
                item.put("fecha", "" );
                return item;


            }

        }else if(this.fecha.equals("f e c h a folio descripcion")){
            item.put("concepto", fecha  );
            item.put("fecha", "" );
            return item;


         }else if(this.fecha.equals("fecha folio descripcion")){
            item.put("concepto", fecha  );
            item.put("fecha", "" );
            return item;


         }

         String folio =  (String) item.get("folio");


         if(Util.hasMonthWithGuion(folio) && folio.indexOf(" ") > -1){

            String[] datos = folio.split(" ");
            String fechatemp = datos[0];
            String folioTemp = datos[1];

            item.put("fecha", fechatemp );
            item.put("folio", folioTemp );


         }

         return item;
    }


   /* public Float getWidth(String key){


        if(key.equals("descripcion")){
            return -0.05f;

        }
 
        
        return 0.0f;

    }*/
    


}