package com.textact.banco;


import java.util.Map;
import java.util.HashMap;
import java.util.List;

import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import com.textact.resources.Util;


import com.textact.pdf.BoundingBoxPDF;


public class Inbursa extends Banco {

    public JSONArray blocks;


    public static JSONObject banco(){
        JSONObject banco = new JSONObject();
        banco.put("value", "inbursa");
        banco.put("label", "Inbursa");
        banco.put("active", true);

        return banco;

    }

    public Inbursa(String jobId){
        super(jobId);

    }

    public String[] getTitles(){
        String[] titles=  {"fecha", "referencia", "concepto", "cargos", "abonos", "saldo"};
        return titles;    
    }

    public String getTitleLeader(){
        return "referencia";
    }

    public void addBlock(JSONObject item){

    }

    public Map<String, String> getKeys(){
        Map<String, String> keys= new HashMap<String, String>();


        keys.put("fecha", "fecha");
        keys.put("abonos", "deposito");
        keys.put("cargos", "retiro");
        keys.put("saldo", "saldo");
        keys.put("concepto", "concepto");
        keys.put("referencia", "referencia");

        return keys;

    }

    public String isDataType(String key, String value){

        System.out.println("key: " + key + " value: "+ value);

        if(key.equals("cargos")){
            String cargos= Util.convertNumberTwoDecimals(value);
            return cargos;
        }


        return value;
    }

    public JSONObject adjustJson(JSONObject json){
        System.out.println("ADJUSTjSON: " + json.toJSONString());


        String deposito = (String) json.get("deposito");
        json.put("deposito", Util.convertNumber(deposito));


        String retiro = (String) json.get("retiro");
        json.put("retiro", Util.convertNumber(retiro));

        String saldo = (String) json.get("saldo");

        //System.out.println(Util.numberWords(saldo));

        if(Util.numberWords(saldo) == 2){
            deposito = Util.getWord(saldo, 0);
            saldo = Util.getWord(saldo, 1);
            json.put("deposito", Util.convertNumber(deposito));

        }


        json.put("saldo", Util.convertNumber(saldo));

        String fecha = (String) json.get("fecha");

        if(Util.hasMonth(fecha)){
            System.out.println("tiene fecha");
            if(Util.numberWords(fecha) > 1){
                fecha = Util.getWord(fecha, 1) + "/" +  Util.getWord(fecha, 0);
                json.put("fecha", fecha);

            }
        }


        System.out.println("ADJUSTjSON FNAL: " + json.toJSONString());

        return json;

    }

    //Es para continuar cuando llega al final
    public Boolean isValidFormatToContinue(JSONObject item){
        System.out.println("IS VALID TO CONTINUE: " + item.toJSONString());

        String concepto = (String) item.get("concepto");
        String referencia = (String) item.get("referencia");
        String deposito= (String) item.get("deposito");
        String saldo= (String) item.get("saldo");

        String retiro= (String) item.get("retiro");

        if(deposito != null){
            deposito= deposito.replaceAll(",", "");
        }

        if(retiro != null){
            retiro= retiro.replaceAll(",", "");
        }

        if(saldo != null){
            saldo= saldo.replaceAll(",", "");
        }

        if(!referencia.equals("")){
           concepto = referencia + " " + concepto; 
        }

        String fecha = (String) item.get("fecha");

        if(fecha.indexOf("Tasas expresadas en")> -1){
            System.out.println("NO CONTINUO 1");
            return false;
        }else if(fecha.indexOf("través de transferencias")> -1){
            System.out.println("NO CONTINUO 2");

            return false;
        }else if(fecha.indexOf("transferencias bancarias electrónicas")> -1){
            System.out.println("NO CONTINUO 3");

            return false;
        }else if(concepto.indexOf("CETES")> -1){
            System.out.println("NO CONTINUO 4");

            return false;
        }else if(Util.isNumber(deposito)){
            return true;
        
        }else if(Util.isNumber(retiro)){
            return true;
        
        }else if(Util.isNumber(saldo)){
            return true;
        
        }else if(fecha.equals("") && concepto.equals("")){
            System.out.println("NO CONTINUO 4");

            return false;
        }

        return true;
    }

    public String[] getTitlesHeader(){
        String[] titles=  {"fecha", "referencia", "concepto", "cargos", "abonos", "saldos"};
        return titles;
    }

    //Rango para buscar la medidas del top
    public Double getBottomHeader(){
        return 0.004;

    }
    public Double getTopHeader(){
        return 0.004;

    }


    public Boolean isCenter(String key){
        return false;
    }



    public Float beginTableOne(List<BoundingBoxPDF> headers, String valorLeader){
        Float topHeader=1.0f;

        for(BoundingBoxPDF box: headers){
            String text= box.getText();
            text= text.toLowerCase().trim();

            if(text.equals(valorLeader)){
                if(topHeader > box.getTop()){
                    topHeader = box.getTop();
                }
            }
            
        }

        return topHeader;



    }


    public Float beginTableSecond(JSONArray blocksJson){
        Float topHeader=100.0f;
        return topHeader;


    }



    public Float getWidth(String key){

        //System.out.println("ENTROOO CON LA LLAVE: " + key);

     
        if(key.equals("cargos")){
            return 0.05f;

        }else if(key.equals("concepto")){
            return 0.08f;

        }
        
        return 0.0f;

    }



}